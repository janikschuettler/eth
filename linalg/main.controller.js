/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {

"use strict";

angular
  .module("app")
  .controller('mainController', mainController);

mainController.$inject = ["$scope", "dataService", "$sce"];

function mainController($scope, dataService, $sce) {

  var self = $scope;

  // var
  self.theoremData = dataService;
  self.data = {};

  self.userIsSearching = self.searchInput != "";
  self.initialStateIsHidden = {
    theorems: true,
    subchapters: true,
    chapters: false
  };
  self.theoremColors = {
    definition: "info",
    proposition: "primary",
    theorem: "danger",
    satz: "danger",
    lemma: "warning",
    korollar: "warning",
    bemerkung: "",
    notiz: ""
  };
  self.userSettings = {
    indentTheorems:false,
    colorTheorems:true,
    showTheoremIdentifier:false,
    showTheoremSummary:true,
    showAdvancedSettings:false,
    showTheoremNaturalness:false
  };


  self.hide = {
    chapters: function() {return setHiddenStateForChapters(true)},
    subchapters: function() {return setHiddenStateForSubchapters(true)},
    theorems: function() {return setHiddenStateForTheorems(true)}
  };

  self.show = {
    chapters: function() {return setHiddenStateForChapters(false)},
    subchapters: function() {return setHiddenStateForSubchapters(false)},
    theorems: function() {return setHiddenStateForTheorems(false)}
  };

  self.safeHTML = safeHTML;

  activate();

  ////////
  Object.defineProperties(self, {});

  self.$watch("userSettings", userSettingsChanged, true);

  ////////
  function activate() 
  {
    self.$root.$watch(function(){
      if (window.MathJax)
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
      return true;
    });
    
    dataService.loadData("__/data/linear_algebra.json")
      .then(function(result) {
        self.data = processData(result);
      });

    if (localStorage.userSettings) {
      self.userSettings = JSON.parse(localStorage.userSettings);
    }
  }

  function safeHTML(text)
  {
    text = text.replace(/\n/g,"<br>");
    return $sce.trustAsHtml(text);
  }

  function processData(data)
  {
    var filledOut = 0,
        notFilledOut = 0;

    data.chapters.forEach(function(chapter)
    {
      chapter.hide = self.initialStateIsHidden.chapters;
      chapter.chapters.forEach(function(subchapter)
      {
        subchapter.hide = self.initialStateIsHidden.subchapters;

        if (subchapter.number && parseFloat(subchapter.number) < 8) {
          if (subchapter.content.length > 0)
            filledOut++
          else
            notFilledOut++
        }

        subchapter.content.forEach(function(theorem) {
          /*var type = theorem.type.split(" ");
          theorem.type = type[0];
          theorem.number = parseInt(type[1]) || "";*/
          theorem.hide = self.initialStateIsHidden.theorems;
        });
      })
    });

    // console.log(JSON.stringify(data));

    console.log("filledOut: " + filledOut + ", notFilledOut: " + notFilledOut);
    console.log(Math.round(100*filledOut/(filledOut+notFilledOut)) + "% filledOut");

    return data;
  }

  function setHiddenStateForChapters(state)
  {
    self.data.chapters.forEach(function(chapter)
    {
      chapter.hide = state ? true : false;
    });
  }

  function setHiddenStateForSubchapters(state)
  {
    self.data.chapters.forEach(function(chapter)
    {
      chapter.chapters.forEach(function(subchapter)
      {
        subchapter.hide = state ? true : false;
      })
    });
  }

  function setHiddenStateForTheorems(state)
  {
    self.data.chapters.forEach(function(chapter)
    {
      chapter.chapters.forEach(function(subchapter)
      {
        subchapter.content.forEach(function(theorem) {
          theorem.hide = state ? true : false;
        });
      })
    });
  }

  function userSettingsChanged()
  {
    localStorage.userSettings = JSON.stringify(self.userSettings);
  }
}





angular.module('app')
.directive('latexPreview', function () {
  return {
    scope: {
      content: "="
    },
    link: function($scope, element) 
    {
      $scope.render = function(content) 
      {
        // console.log("render");
        element.html(content);
      };
    },
    template: "{{render(content)}}"
  };
});


})();

