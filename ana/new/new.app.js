/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

/*
 * TODO
 * implement config file
 * better system for automatic dropdown closing
 * delete entry upon partial fail
 * itemize, qm, textbf in plain text
 * add new patterns to current pattern list
 * include input max lengths
 * errors in service data fetch
 * sort patterns by frequence of use
*/


angular.module('app', []);

angular.module('app').config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    "self",
    // Allow loading from our assets domain.  Notice the difference between * and **.
    "http://www.video.ethz.ch/etc/designs/mmp/paella/video.html/**",
    "http://js.vsos.ethz.ch/**"
  ]);
});



/*
var data1 = [{
  tex: "$f:[a,b] \to \R$ eine stetige Funktion.",
  id: 1
}, {
  tex: "Test",
  id: 2
}, {
  tex: "$f:[a,b] \to \R$ ist ein kompaktes Intervall.",
  id: 3
}, {
  tex: "Seien $X,Y$ zwei Mengen mit $X \lesssim Y$ und $Y \lesssim X$.",
  id: 4
}, {
  tex: "Dann gibt es injektive Funktionen $f:X \to Y$ und $g: Y \to X$.",
  id: 5
}, {
  tex: "Eine Menge $X$ heisst \textbf{überabzählbar}, falls $\N$ schmächtiger ist als $X$, aber $\N$ nicht gleichmächtig zu $X$ ist.",
  id: 6
}];

var data2 = [{
  id: 1,
  name: "Bolzano-Weiserstrass",
  latex_label: "bolzanoweiserstrass",
  lecture_note_chapter: "5.4",
  lecture_note_id: "5.4.46",
  summary: "Existenz von Minimum und Maximum",
  type: 1
}, {
  id: 2,
  name: "",
  latex_label: "somesatz",
  lecture_note_chapter: "2.3",
  lecture_note_id: "2.3.21",
  summary: "Konstruktion der reellen Zahlen",
  type: 1
}, {
  id: 3,
  name: "Zwischenwertsatz",
  latex_label: "zwischenwert",
  lecture_note_chapter: "6.1",
  lecture_note_id: "6.1.5",
  summary: "Zwischenwerte einer stetigen Funktion",
  type: 1
}]

var latexLabels = ["hallo", "test", "foo", "bar"];*/

/*var lectures = [{
    lecture_date: "",
    video_id: "82be3461-1805-318a-bc11-bd74098420f6"
  }, {
    lecture_date: "",
    video_id: "a2d2b760-164f-34be-9d5b-d8dbd088a45f"
  }, {
    lecture_date: "",
    video_id: "ed6057b5-e89f-3e15-b9f8-3ed6011754bb"
  }];*/


/*var lecturesAnaII = [
  {id: 61, lecture_date:"2017-04-06", video_id:"aa86e580-6031-324a-b45c-414298c593d7", part: 2},
  {id: 60, lecture_date:"2017-04-05", video_id:"86427e21-0e30-3f72-96c0-8eb3465efe62", part: 2},
  {id: 59, lecture_date:"2017-04-03", video_id:"ea60007f-6511-36e5-9dea-a92bc94b1e35", part: 2},
  {id: 58, lecture_date:"2017-03-30", video_id:"a2d2b760-164f-34be-9d5b-d8dbd088a45f", part: 2},
  {id: 57, lecture_date:"2017-03-29", video_id:"ed6057b5-e89f-3e15-b9f8-3ed6011754bb", part: 2},
  {id: 56, lecture_date:"2017-03-27", video_id:"7a122756-ecae-3dd2-8906-075ed8c89fd8", part: 2},
  {id: 55, lecture_date:"2017-03-23", video_id:"8bed2bd7-cb76-37db-a42b-b37ebfeadf09", part: 2},
  {id: 54, lecture_date:"2017-03-22", video_id:"526f0a56-4d72-3ee0-9f88-512cb7d1fb7d", part: 2},
  {id: 53, lecture_date:"2017-03-20", video_id:"d10a28e6-2962-36cf-9839-cfc72db1cf84", part: 2},
  {id: 52, lecture_date:"2017-03-16", video_id:"82be3461-1805-318a-bc11-bd74098420f6", part: 2},
  {id: 51, lecture_date:"2017-03-15", video_id:"5a0d4a87-c40c-3072-bb90-f5b0808850d6", part: 2},
  {id: 50, lecture_date:"2017-03-13", video_id:"719a3b72-19b9-340b-827d-8950fc4489db", part: 2},
  {id: 49, lecture_date:"2017-03-09", video_id:"a349286e-33a5-36ba-921e-1b70d0674464", part: 2},
  {id: 48, lecture_date:"2017-03-08", video_id:"e97697f4-522f-3f0e-b83b-cf191c6cd177", part: 2},
  {id: 47, lecture_date:"2017-03-06", video_id:"82ec4aca-c2fd-3b51-a10e-de0b986a5e0c", part: 2},
  {id: 46, lecture_date:"2017-03-02", video_id:"ebc8321b-648b-3cba-8148-a1998e8efa33", part: 2},
  {id: 45, lecture_date:"2017-03-01", video_id:"35956e3c-7739-39f0-bb71-caddc85fde8c", part: 2},
  {id: 44, lecture_date:"2017-02-27", video_id:"488dd792-bd3f-3666-a995-832ab780a0a7", part: 2},
  {id: 43, lecture_date:"2017-02-23", video_id:"f98e5a4b-757a-3cd7-a804-9de2935ec261", part: 2},
  {id: 42, lecture_date:"2017-02-22", video_id:"33a7408b-f52f-3952-b602-d30aea3b4ee3", part: 2},
  {id: 41, lecture_date:"2017-02-20", video_id:"eddea48b-a247-4918-92ea-ceb6ad3f5a1e", part: 2}
];

var lecturesAnaI = [
  {id: 40, lecture_date:"2016-12-22", video_id:"610a028f-f399-36b6-8091-64559abcb123", part: 1},
  {id: 39, lecture_date:"2016-12-21", video_id:"05183157-90f0-3bf7-aeee-a39f58bf7f6c", part: 1},
  {id: 38, lecture_date:"2016-12-19", video_id:"f035d0ed-2cca-3ca0-9f34-dd7f73ecfbb7", part: 1},
  {id: 37, lecture_date:"2016-12-15", video_id:"3af9dfe1-057d-35c2-a842-44282394680d", part: 1},
  {id: 36, lecture_date:"2016-12-14", video_id:"ccb7aa42-e49f-3dcd-b30a-d268d027c896", part: 1},
  {id: 35, lecture_date:"2016-12-08", video_id:"aad5365f-19a0-3eeb-885b-9aef151dfaaa", part: 1},
  {id: 34, lecture_date:"2016-12-07", video_id:"0142f884-8b4a-3a04-9217-75f7f7c7ef5c", part: 1},
  {id: 33, lecture_date:"2016-12-05", video_id:"2a327b17-4b48-3c02-80be-a8f321400f2f", part: 1},
  {id: 32, lecture_date:"2016-12-01", video_id:"23ffe085-0344-3b94-b17b-5f0a5efa0f9f", part: 1},
  {id: 31, lecture_date:"2016-11-30", video_id:"362d566b-505b-3c11-bf78-d3be0a7f4268", part: 1},
  {id: 30, lecture_date:"2016-11-28", video_id:"694407a6-f79c-30c9-9bcd-f72b80ed775f", part: 1},
  {id: 20, lecture_date:"2016-11-24", video_id:"9224687d-141d-397a-a02a-041ee58904f8", part: 1},
  {id: 28, lecture_date:"2016-11-23", video_id:"bde06507-10c6-3478-9bde-5e8275ef4a92", part: 1},
  {id: 27, lecture_date:"2016-11-21", video_id:"80f79bc0-3625-3952-8410-7dcc7ca8960a", part: 1},
  {id: 26, lecture_date:"2016-11-17", video_id:"432dedac-7ad8-349b-87b2-f66e8724e97c", part: 1},
  {id: 25, lecture_date:"2016-11-16", video_id:"6d899b02-8927-32b8-8e7f-f140c6ee2699", part: 1},
  {id: 24, lecture_date:"2016-11-14", video_id:"5a4eb02b-509d-3237-8388-3686d1f822f2", part: 1},
  {id: 23, lecture_date:"2016-11-10", video_id:"f4096ce4-951e-47bd-ac08-94c8c6c0500e", part: 1},
  {id: 22, lecture_date:"2016-11-10", video_id:"1c0fd9f7-73bb-3030-a61c-340883feda52", part: 1},
  {id: 21, lecture_date:"2016-11-07", video_id:"9aceecf5-f8f2-3005-bfb1-855b635ad250", part: 1},
  {id: 20, lecture_date:"2016-11-03", video_id:"296d5191-acac-3af7-a84c-a2b179fb9eb3", part: 1},
  {id: 19, lecture_date:"2016-11-02", video_id:"c1748e62-2b64-30ec-b4c0-96f675f4b5a5", part: 1},
  {id: 18, lecture_date:"2016-10-31", video_id:"2308cf51-2882-3053-bdb9-390f72eafbee", part: 1},
  {id: 17, lecture_date:"2016-10-27", video_id:"b307b199-153e-328e-8c92-b15e337c1f13", part: 1},
  {id: 16, lecture_date:"2016-10-26", video_id:"2c985c1a-ac93-358e-b99c-459dbc0784da", part: 1},
  {id: 15, lecture_date:"2016-10-26", video_id:"580c7545-c328-326d-a5e6-0385710182cb", part: 1},
  {id: 14, lecture_date:"2016-10-24", video_id:"c0e36c8c-7978-354a-bccf-4ef3791ea06d", part: 1},
  {id: 13, lecture_date:"2016-10-20", video_id:"3dd27228-39be-36a5-9e09-8d0c2d78ad3a", part: 1},
  {id: 12, lecture_date:"2016-10-19", video_id:"74716db8-5ead-3bc4-bda2-6ffb78510d45", part: 1},
  {id: 11, lecture_date:"2016-10-13", video_id:"2ee5a831-af2b-31ef-8900-7d3a112952ff", part: 1},
  {id: 10, lecture_date:"2016-10-12", video_id:"b8f65259-7596-3d86-bb68-cc95063a554f", part: 1},
  {id: 9, lecture_date:"2016-10-10", video_id:"5220c5a6-a4d7-33f5-9a8a-12cf7ff55a50", part: 1},
  {id: 8, lecture_date:"2016-10-06", video_id:"5c05d7d5-d6e8-3cdd-829c-23c631f02a05", part: 1},
  {id: 7, lecture_date:"2016-10-05", video_id:"b5308ffb-ca26-35a3-a953-6792f9662276", part: 1},
  {id: 6, lecture_date:"2016-10-03", video_id:"edfedcab-4502-35f4-b544-f8d5ef5d01ec", part: 1},
  {id: 5, lecture_date:"2016-09-29", video_id:"5bf21b4c-0212-3b87-bb45-417ab2e7f4dc", part: 1},
  {id: 4, lecture_date:"2016-09-28", video_id:"13038b7e-fca2-3704-a78e-1278ca58b1b4", part: 1},
  {id: 3, lecture_date:"2016-09-26", video_id:"f9dbff54-8594-35ca-bd35-acfebacfd2a6", part: 1},
  {id: 2, lecture_date:"2016-09-22", video_id:"f3ace887-652a-383b-8035-06b24c58bf96", part: 1},
  {id: 1, lecture_date:"2016-09-21", video_id:"a65b10a4-c135-3bca-a4f5-c37d9f8b0913", part: 1}
];
var lectures = lecturesAnaII.concat(lecturesAnaI).reverse();

// INSERT INTO `analysis_lectures` (`id`, `lecture_date`, `video_id`) VALUES ('1', '2017-04-03', 'dsadadsad');

var sql = "";

lectures.forEach(function(lecture) {
  sql += "INSERT INTO `analysis_lectures` (`id`, `lecture_date`, `video_id`) VALUES ('" + lecture.id + "', '" + lecture.lecture_date + "', '" + lecture.video_id + "');\n";
})*/
