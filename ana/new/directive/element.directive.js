(function() {

angular.module("app")
.directive("elementDirective", function(idHandler) 
{
  return {
    require: "?ngModel",
    restrict: "E",
    // link: ["idHandler", elementDirective],
    link: function($scope, element, attibutes, ngModel) {
      return elementDirective($scope, element, attibutes, ngModel, idHandler);
    },
    transclude: true,
    scope: {
      data: "=",
      hideAddNewOption: "=",
      type: "@"
    },
    templateUrl: "directive/element.directive.template.html"
  };
});


function elementDirective($scope, element, attibutes, ngModel, idHandler) {

  "use strict";

  var self = $scope;

  ////////

  // vars
  self.dropdownIsVisible = false;
  self.itemIsPreviewed = false;

  self.properties = {
    name: attibutes.type,
    previewTex: ""
  };

  self.useExistingData = self.type.indexOf("existing") > -1;
  self.useNewData = self.type.indexOf("new") > -1;

  self.noSearchResultsText = "Keine Ergebnisse";
  self.placeholderText = "Hinzufügen";
  self.addNewText = "Neu";

  // self.data = data;
  self.values = [];

  self.idAutoIncrement = 1;

  // functions
  self.searchInputWasChanged    = searchInputWasChanged;
  self.searchInputWasFocused    = searchInputWasFocused;
  self.searchInputWasBlurred    = searchInputWasBlurred;
  self.dropdownItemWasClicked   = dropdownItemWasClicked
  self.createNewItemWasClicked  = createNewItemWasClicked
  self.clearSearchInput         = clearSearchInput
  self.addNewItem               = addNewItem;
  self.removeItem               = removeItem;

  updateModel();


  Object.defineProperties(self, {
      "dropdownIsHidden": {
          get: function() {
              console.warn("WE DO NEED this function");
              return self.dropdownIsVisible && !self.itemIsPreviewed ? "z-index: 1000" : "opacity: 0; z-index: -10;";// "opacity: 0; z-index: 0;"; // "z-index: -100; opacity: 0;";
          }
      }
  });

  ////////

  function activate() 
  {
    updateModel();
    console.log(self.type);
    console.log(self.data);
  }

  self.$watch(ngModel, updateModelWithValue);
  ngModel.$render = updateModelWithValue;

  function searchInputWasChanged() 
  {
    self.previewTex = self.searchInput;
  }

  function searchInputWasFocused(event) 
  {
    self.dropdownIsVisible = self.useExistingData ? true : false;
  }

  function searchInputWasBlurred(event) 
  {
    // this function is pain in the ass as is seems to be evoked before the onclick event handlers
    // for the dropdown menu items and therefore essentially kills these handlers
    return;
    window.setTimeout(function() {
      self.dropdownIsVisible = false;
      self.$apply();
    }, 50);
  }

  function dropdownItemWasClicked(object) 
  {
    console.log("adding new item ");
    addItem(object);
  }

  function createNewItemWasClicked() 
  {
    self.itemIsPreviewed = true;
    hideDropdown();
  }

  function clearSearchInput()
  {
    self.searchInput = "";
    self.itemIsPreviewed = false;
    self.previewTex = "";
    hideDropdown();
  }

  function addNewItem() 
  {
    if (!self.previewTex)
      return;

    addItem({
      // note: "Some new Item",
      tex: self.previewTex
    });
  }

  function removeItem(object)
  {
    var index = self.values.length;

    self.values = self.values.filter(function(element, i) {

      if (element.number == object.number) {
      // if (element.id == object.id) {
        index = i;
        return false;
      }

      if (i > index) {
        element.number -= 1;
      }

      return true;
    });

    console.log(self.values);

    updateModel();
  }

  function addItem(object) 
  {

    var newItem = {}; 
    newItem.originalObject = angular.copy(object);
    newItem.originalObject.id = object.id || idHandler.generateTempId();
    newItem.number = self.values.length + 1;

    self.values.push(newItem);

    console.log(self.values);

    clearSearchInput();
    updateModel();  
  }

  function reset()
  {
    self.values = [];
    self.idAutoIncrement = 1;
    self.dropdownIsVisible = false;
    self.itemIsPreviewed = false;
  }

  function updateModel()
  { 
    var filteredValues = self.values.map(function(element) {
      return element.originalObject;
    });
    // remove angular-owned properties like $$hashKey
    filteredValues = JSON.parse(angular.toJson(filteredValues));

    if (ngModel) 
      ngModel.$setViewValue(filteredValues);
  }

  function updateModelWithValue() 
  {
    var newValue = ngModel.$viewValue;
    self.values  = newValue;
  }

  function hideDropdown()
  {
    self.dropdownIsVisible = false;
  }
}


})();