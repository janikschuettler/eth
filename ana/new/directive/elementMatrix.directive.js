(function() {
	
angular.module('app')
.directive('elementMatrixDirective', function(idHandler) 
{
  return {
    require: '?ngModel',
    restrict: "E",
    link: function($scope, element, attibutes, ngModel) {
      return elementMatrixDirective($scope, element, attibutes, ngModel, idHandler);
    },
    scope: {
      premises: "=",
      statements: "="
    },
    templateUrl: "directive/elementMatrix.directive.template.html"
  };
});

function elementMatrixDirective($scope, element, attibutes, ngModel, idHandler) 
{
  "use strict";

  var self = $scope;

  // self.matrix = [[1]];
  self.matrix = {};
  self.dimensions = {
    x: 0,
    y: 0
  };

  self.elementWasClicked = elementWasClicked;
  self.elementClass = elementClass;

  activate();

  ////////

  function activate() {}

  self.$watch("premises", premisesChanged, true);
  self.$watch("statements", statementsChanged, true);

  ////////

  function elementWasClicked(premiseId, statementId) 
  {
    self.matrix[premiseId][statementId] = self.matrix[premiseId][statementId] ? 0 : 1;
    updateModel();
  }

  function elementClass(premiseId, statementId) 
  {
    return self.matrix[premiseId][statementId] ? "btn-primary" : "btn-secondary";
  }

  function premisesChanged(newValue, oldValue)
  {
    if (!newValue)
      return;

    if (newValue.length > self.dimensions.x)
    {
      var newMatrixElement = {};
      self.statements.forEach(function(premise) {
        newMatrixElement[premise.id] = 1;
      });
      self.matrix[newValue[newValue.length-1].id] = newMatrixElement;
      self.dimensions.x++;
    }
    else if (newValue.length < self.dimensions.x)
    {
      delete self.matrix[getKeyOfRemovedRow(newValue, oldValue)];
      self.dimensions.x--;
    }

    updateModel();
  }

  function statementsChanged(newValue, oldValue)
  {
    if (!newValue)
      return;
    
    if (newValue.length > self.dimensions.y)
    {
      for (var key in self.matrix)
        self.matrix[key][newValue[newValue.length-1].id] = 1;

      self.dimensions.y++;
    }
    else if (newValue.length < self.dimensions.y)
    {
      for (var key in self.matrix)
        delete self.matrix[key][getKeyOfRemovedRow(newValue, oldValue)];

      self.dimensions.y--;
    }

    updateModel();
  }

  function getKeyOfRemovedRow(newValue, oldValue)
  {
    for (var i = 0; i < newValue.length; i++)
    {
      if (newValue[i].id != oldValue[i].id)
        return oldValue[i].id;
    }
    return oldValue[oldValue.length-1].id;
    /*
    for (var key in oldValue)
    {
      if (newValue[key].id != oldValue[key].id)
        return key;
    }
    return false;*/
  }

  function getIndexOfRemovedRow2(newValue, oldValue)
  {
    for (var i = 0; i < newValue.length; i++)
    {
      if (newValue[i].id != oldValue[i].id)
        return i;
    }
    return oldValue.length;
  }

  function updateModel()
  {
    var data = [];
    for (var premise in self.matrix)
      for (var statement in self.matrix[premise])
        if (self.matrix[premise][statement])
          data.push({
            id: idHandler.generateTempId(),
            premise_pattern_id: premise,
            statement_pattern_id: statement
          });

    if (ngModel)
      ngModel.$setViewValue(data);
  }
}


})();