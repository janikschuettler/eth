/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {

"use strict";

angular
  .module('app')
  .controller('newTheoremController', newTheoremController);

newTheoremController.$inject = ["$scope", "$sce", "newTheoremService", "dataService", "idHandler", "$timeout", "latexService", "sendMail"];

function newTheoremController($scope, $sce, newTheoremService, dataService, idHandler, $timeout, latexService, sendMail) {

  var self = $scope;

  // data
  self.form = {};

  // data/ service
  self.newTheoremService = newTheoremService;

  // progress
  self.requestStatus = {};

  self.lectureBaseUrl = "http://www.video.ethz.ch/etc/designs/mmp/paella/video.html";
  self.lectureUrl = "";
  self.latexLabelIsUnique = true;
  self.editMode = false;

  self.userFeedback = {};

  // func
  self.create = create;
  self.latexLabelInputWasChanged = latexLabelInputWasChanged;
  self.renderLectureUrl = renderLectureUrl;
  self.loadTheorem = loadTheorem;

  activate();

  ////////
  Object.defineProperties(self, {});

  ////////
  function activate() 
  {
    self.$root.$watch(latexService.updateLatex);
    resetForm();
    renderLectureUrl();
  }

  self.$watch("form", formChanged, true);

  function formIsValid()
  {
    self.userFeedback = {};

    var formIsValid = 
      self.latexLabelIsUnique &&
      // self.form.theorems.lecture_note_id &&
      (self.form.theorems.summary || self.form.theorems.name) &&
      self.form.theorems.type &&
      self.form.theorems.importance &&
      self.form.theorems.lecture &&
      self.form.statements.length > 0 &&
      (self.form.premises.length == 0 || self.form.premises_to_statements.length > 0);

    var inputIsMissingErrorObject = {
      message: "Dieses Feld darf nicht leer sein.",
      classLabel: "has-danger"
    };

    console.log(self.form.premises_to_statements);
    console.log((self.form.premises.length == 0 || self.form.premises_to_statements.length > 0));

    if (!self.latexLabelIsUnique) {}
    // if (!self.form.theorems.lecture_note_id) {
    //   self.userFeedback.lecture_note_id = inputIsMissingErrorObject;
    // }
    if (!(self.form.theorems.summary || self.form.theorems.name)) {
      var atLeastOneInputErrorObject = {
        message: "Mindestens Name oder Zusammenfassung muss ausgefüllt sein.",
        classLabel: "has-danger"
      };
      self.userFeedback.name = atLeastOneInputErrorObject;
      self.userFeedback.summary = atLeastOneInputErrorObject;
    }
    if (!self.form.theorems.type) {
      self.userFeedback.type = inputIsMissingErrorObject;
    }
    if (!self.form.theorems.importance) {
      self.userFeedback.importance = inputIsMissingErrorObject;
    }
    if (!self.form.theorems.lecture) {
      self.userFeedback.lecture = inputIsMissingErrorObject;
    }
    if (self.form.statements.length == 0) {
      self.userFeedback.statements = inputIsMissingErrorObject;
    }
    if (!(self.form.premises.length == 0 || self.form.premises_to_statements.length > 0)) {
      self.userFeedback.premises_to_statements = {
        message: "Die Voraussetzungen müssen mindestens eine Verbindung zu Aussagen haben.",
        classLabel: "has-danger"
      };
    }

    return formIsValid;
  }

  function create() 
  {
    // write directives to check if form is valid and to show or disable create button accordingly
    if (!formIsValid())
    {
      console.log("form not valid");
      return;
    }

    console.log("form valid");

    // return;

    self.creationIsInProgress = true;
    self.requestStatus = {};

    var progressPromiseChain = newTheoremService.createNewTheorem(self.form);

    progressPromiseChain
      .then(function(result) {

        self.requestStatus.success = true;
        self.requestStatus.userHeading = "Eingabe war erfolgreich";
        self.requestStatus.userMessage = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus aliquid, enim delectus ex fugit nihil voluptatum harum, accusantium rem voluptatem quis quasi fuga numquam recusandae, cum a facilis magni praesentium.";
        console.log("Insert was successful");

        resetForm();

      }, function(result) {

        var failedRequestObject = result.responses[result.responses.length-1];
        self.requestStatus.success = false;
        self.requestStatus.userHeading = "Eingabe ist fehlgeschlagen";
        self.requestStatus.userMessage = failedRequestObject.response.error + " [" + failedRequestObject.request.config.data.sql + "]";
        console.error("Insert failed");

        var errorMessage = "Error Message: \n \n";
            errorMessage += JSON.stringify(failedRequestObject.response, null, 4);
            errorMessage += "\n \n For request: \n \n:";
            errorMessage += JSON.stringify(failedRequestObject.request.config.data);

        sendMail.send("Error with 'New | Ana'", errorMessage);
      })
      .finally(function() {
        self.requestStatus.done = true;
        $timeout(function() {
          self.requestStatus.done = false;
        }, 10 * 1000);
      });
  }

  function latexLabelInputWasChanged()
  {
    if (self.newTheoremService.data.latexLabels.indexOf(self.form.theorems.latex_label) > -1){
      self.latexLabelIsUnique = false;
    }
    else {
      self.latexLabelIsUnique = true;
    }
  }

  function loadTheorem(theorem)
  {
    self.editMode = true;
    
    var theoremObject = dataService.getTheoremObject(theorem.id);
    self.form.theorems = theoremObject.theorem;
    self.form.proofs = theoremObject.proofs[0];

    renderLectureUrl();
  }

  function renderLectureUrl()
  {
    var newId   = parseInt(self.form.theorems.lecture_id) - 1,
        lecture = self.newTheoremService.data.lectures[newId];

    console.log(newId);

    if (!lecture) {
      return;
    }

    self.lectureUrl = $sce.trustAsResourceUrl(self.lectureBaseUrl + "?id=" + lecture.video_id + "&time=" + self.form.theorems.video_position);
  }

  function formChanged()
  {
    // console.log("form changed");
    // if (self.requestStatus.done)
    //   self.requestStatus = {};
  }

  function resetForm()
  {
    var proofId   = idHandler.generateTempId(),
        theoremId = idHandler.generateTempId();

    angular.element(document.querySelector("#proofPreview")).empty();

    self.form = {
      theorems: {
        id: theoremId,
        name: "",
        summary: "",
        type: "",
        lecture_note_id: "",
        latex_label: "",
        lecture_note_chapter: "",
        lecture: "",
        importance: "",
        video_position: ""
      }, 
      proofs: {
        id: proofId,
        tex: "",
        short: ""
      }, 
      theorems_to_proofs: [{
        id: idHandler.generateTempId(),
        theorem_id: theoremId,
        proofs_id: proofId
      }],
      notes: [],
      premises: [],
      statements: [],
      proofs_to_definitions: [],
      proofs_to_theorems: []
    };
  }
}

})();

