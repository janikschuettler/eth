/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

(function() {
"use strict";

angular
  .module("app")
  .controller("GraphViewController", GraphViewController);

GraphViewController.$inject = ["$scope", "dataService", "settings", "$routeParams"];

function GraphViewController($scope, dataService, settings, $routeParams) 
{
  var self = $scope;
  self.title = "GraphViewController";

  var network;

  var defaultSettings = {
    showArrows: true,
    hideUnconnected: true,
    scaleDotSize: true,
    exists: "yes",
    layout: "0",
    hideInduction: false,
    colorCatalog: true,
    chapter: ""
  };

  self.hideNavigation = true;

  self.userSettings   = settings.graph ? angular.copy(settings.graph) : defaultSettings;
  self.chapterFilter  = chapterFilter;
  self.picturizeGraph = picturizeGraph;

  self.$on("databaseFinishedLoading", onload);

  $scope.$on('$routeChangeSuccess', function() {
    self.userSettings.chapter = $routeParams.studyChapter || self.userSettings.chapter;
    self.userSettings.chapter = self.userSettings.chapter == "All" ? "" : self.userSettings.chapter;

    self.hideNavigation = $routeParams.hideNavigation === "true";

    console.log($routeParams);

    console.log(self.userSettings);

    if (dataService.isLoaded) onload();
  });

  activate();

  ////////////////

  function activate() 
  {

    if (dataService.isLoaded)
      onload();

    self.$watch("userSettings", userSettingsChanged, true);
  }

  function onload() 
  {
    createGraph(dataService, dataService.theorems.data);
    self.chapters = dataService.chapters.data;
  }

  function userSettingsChanged(newValue)
  {
    if (dataService.isLoaded)
      // updateOptions();
      onload();

    settings.graph = {};

    for (var key in newValue)
    {
      settings.graph[key] = newValue[key];
    }
  }

  function createGraph(data, nodesData, edgesData)
  {
    var nodesUsed = [];

    var addToNodesUsed = function(nodeId) {
      if (nodesUsed.indexOf(nodeId) < 0)
        nodesUsed.push(nodeId);
    }

    // create an array with edges
    var edges = new vis.DataSet(data.proofs_to_theorems.data.map(function(element) {

      var to = element.theorem_id,
        from = -1;

      if (element.proof_id != data.theorems_to_proofs[element.proof_id].id) {
        for (var i = 0; i < data.theorems_to_proofs.length; i++) {
          var subElement = data.theorems_to_proofs.data[i];
          if (subElement.proof_id == element.proof_id) {
            from = subElement.theorem_id;
            break;
          }
        }
      }
      else
        from = data.theorems_to_proofs[element.proof_id].theorem_id

      addToNodesUsed(from);
      addToNodesUsed(element.theorem_id);

      return (to == "22" && self.userSettings.hideInduction) ? {} : {
        to: to,
        from: from
      };
    }));

    var splitter = function(str, l){
      var strs = [];
      while(str.length > l)
      {
        var pos = str.substring(0, l).lastIndexOf(' ');
        pos = pos <= 0 ? l : pos;
        strs.push(str.substring(0, pos));
        var i = str.indexOf(' ', pos)+1;
        if(i < pos || i > pos+l)
            i = pos;
        str = str.substring(i);
      }
      strs.push(str);
      return strs.join("\n");
    }

    // var chapter = "2";

    var numberOfNodes = 0;

    var nodes = new vis.DataSet(nodesData.filter(function(element) 
    {
      var isUsedRestriction = (self.userSettings.hideUnconnected) ? nodesUsed.indexOf(element.id) > -1 : true;
      var isInChapterRestriction = self.userSettings.chapter ? dataService.chapters[element.chapter_id].number.split(".")[0] == self.userSettings.chapter : true;
      return isUsedRestriction && isInChapterRestriction;
    }).map(function(element) 
    {
      numberOfNodes++;
      var label   = data.types_of_theorems[element.type_id].type + " " + element.lecture_note_id + "\n" + splitter(element.name || element.summary, 30) /*.replace(/([^\0]{25})/g, '$1\n')*/ || "No name",
          chapter = parseInt(dataService.chapters[element.chapter_id].number.split(".")[0]);
      var color = "rgb(" + Math.round(255 / 14 * chapter) + ", " + Math.round(255 - 255 / 14 * chapter) + ", " + Math.round(125 / 14 * chapter + 120) + ")";
          // color = self.userSettings.colorCatalog && element.is_in_catalog == "1" ? "rgb(255,0,0)" : color;
      return {
        id: element.id,
        title: label,
        level: self.userSettings.layout == "2" ? chapter : undefined,
        value: element.importance,
        label: label,
        color: color,
        font: {
          color: "white"
        },
        shape: "box"
      };
    }));

    // create a network
    var container = document.getElementById('mynetwork');

    // provide the data in the vis format
    var data = {
        nodes: nodes,
        edges: edges
    };

    // initialize your network!
    network = new vis.Network(container, data, getOptions(numberOfNodes));
  }


  function getOptions(numberOfNodes) 
  {
    var options = {
      nodes: {
        shape: 'dot',
        scaling: {
          customScalingFunction: function (min, max, total, value) {
            return self.userSettings.scaleDotSize ? Math.pow(value/max, 3) : (min + 0) / 1;
          },
          min: 5,
          max: 20
        },
        font: {
          size: 12,
          face: 'Tahoma'
        }
      },
      edges: {
        color: {inherit:true},
        width: .5,
        smooth: {
          type: 'continuous'
        },
        arrows: {from : self.userSettings.showArrows}
      },
      layout: {
        improvedLayout: false
      },
      physics: {
        solver: "hierarchicalRepulsion", //'hierarchicalRepulsion',

        repulsion: {
          centralGravity: 0.2,
          springLength: 200,
          springConstant: 0.05,
          nodeDistance: 100,
          damping: 0.09
        },
        /*repulsion: {
          centralGravity: 0.2,
          springLength: 500,
          // springConstant: 100,
          nodeDistance: 200,
          damping: 0.9
        },*//*,
        repulsion: {
          nodeDistance: 2
        }*/ 
        hierarchicalRepulsion: {
          centralGravity: 0,
          springLength: numberOfNodes * 5 + 80 || 500,
          springConstant: 0.001,
          nodeDistance: 100,
          damping: 0.09
        },
        // enabled: false
      },
      interaction: {
        hideEdgesOnDrag: false,
        tooltipDelay: 200
      }
    };

    if (self.userSettings.layout != "0") 
    {
      var layout = self.userSettings.layout == "3" ? "directed" : "hubsize";
      var direction = self.userSettings.layout == "3" ? "DU" : "UD";
      options.layout.hierarchical = {
        direction: direction,
        sortMethod: layout
      };
    }

    return options;
  }

  function updateOptions()
  {
    network.setOptions(getOptions());
  }

  function chapterFilter(item) 
  { 
    return item.number.split(".").length < 2; 
  }

  function picturizeGraph(event)  
  {
    var dataUrl = document.querySelector("#mynetwork .vis-network canvas").toDataURL();
        // dataUrl = dataUrl.replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');

    var title = "analysis graph";
        title += self.userSettings.chapter ? ", chapter " + self.userSettings.chapter : "";
        title += ".png";

    var downloadLink = angular.element("<a></a>");
        downloadLink.attr("href", dataUrl);
        downloadLink.attr("target", "_blank");
        downloadLink.attr("download", title);
        downloadLink[0].click();  

    return dataUrl;

    var title = "Manfred Einsiedler - Analysis I und II";
        title += self.userSettings.chapter ? ", Kapitel " + self.userSettings.chapter : "";

    var windowContent = '<!DOCTYPE html>';
        windowContent += '<html>'
        windowContent += '<head><title>' + title + '</title></head>';
        windowContent += '<body>'
        windowContent += '<img src="' + dataUrl + '" style="max-height: 100%; max-width: 100%">';
        windowContent += '</body>';
        windowContent += '</html>';

    var printWin = window.open('','','width=500,height=500');
        printWin.document.open();
        printWin.document.write(windowContent);
        printWin.document.close();
        printWin.focus();

    setTimeout(function() {
        printWin.print();
        printWin.close();
    }, 1);
  }
}
})();
