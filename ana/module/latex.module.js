/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


// This module 
(function() {
"use strict";

angular.module("latex", ["katex"])
  .provider("latexConfig", latexConfigProvider);

latexConfigProvider.$inject = ["katexConfigProvider"];


function latexConfigProvider(katexConfigProvider) {

  var parser = new Parser();

  var service = {
    $get: function() {
      return service;
    },
    configure: function(options) {
      katexConfigProvider.configure(window.katexConfig, process);
    },
    refLinkHandler: function(theorem) {return ""},
    refLabels: {}
  };

  // processes latex font variations like \textbf and lists
  // before actual tex rendering
  function process(text)
  {
    if (!text)
      return "";

    var keywords = [
      {
        openTag: "\\textbf{",
        closeTag: "}",
        handler: function(text) {return "<b>" + text + "</b>"}
      }, {
        openTag: "\\emph{",
        closeTag: "}",
        handler: function(text) {return "<i>" + text + "</i>"}
      }, {
        openTag: "\\qm{",
        closeTag: "}",
        handler: function(text) {return "\"" + text + "\""}
      }, {
        openTag: "\\begin{itemize}",
        closeTag: "\\end{itemize}",
        handler: handler.listKeywordHandler
      }, {
        openTag: "\\begin{enumerate}",
        closeTag: "\\end{enumerate}",
        handler: handler.listKeywordHandler
      }, {
        openTag: "\\setcounter{enumi}{",
        closeTag: "}",
        handler: function() {return ""}
      }, {
        openTag: "\\index[symb]{",
        closeTag: "}",
        handler: function() {return ""}
      }, {
        openTag: "\\ref{",
        closeTag: "}",
        handler: handler.refHandler
      }, /*{
        openTag: "\\nomenclature{",
        closeTag: "}",
        handler: function() {return ""}
      },*/ {
        openTag: "\\label{",
        closeTag: "}",
        handler: function() {return ""}
      }
    ];

    text = text.replace(/</g, "&lt;");
    text = text.replace(/>/g, "&gt;");
    text = text.replace(/~/g, "&nbsp;"); // non breaking space
    text = text.replace(/\\enumnewline/g, "");
    text = text.replace(/\\setcounter{enumi}{[0-9]+}/g, "");

    var parsed = parser.parse(text, keywords);

    parsed = parsed.replace(/\n\s*\n/, "<br>");

    return parsed || text;
  }

  var handler = {
    listKeywordHandler: function(text, object) 
    {
      var newText = "<ul>";

      if (text[0] == "[") 
      {
        var bracketSplit = text.split("]");
        var bracketContent = bracketSplit[0].substr(1);
        text = text.replace("[" + bracketContent + "]", "").trim();
      }

      // TODO remove [(i)] from beginning

      var items = text.split("\\item");
      items.forEach(function(item) {
        item = item.trim();
        if (item)
          newText += "<li>" + item + "</li>";
      });

      newText += "</ul>";

      return newText;
    },
    refHandler: function(label)
    {
      var theorem = service.refLabels[label];
      // return service.refLabels[label] ? "<a href='../detail/" + theorem.id + "'>" + service.refLabels[label].lecture_note_id + "</a>" : label;
      return service.refLabels[label] ? service.refLinkHandler(label, theorem) : label;
    }
  };
  
  return service;
}

function Parser()
{
  var self = this;

  self.keyword = {};

  self.parse = parse;

  ////////////////

  function parse(string, keywords)
  {
    return looper(string, keywords || self.keywords);
  }

  function looper(string, keywords)
  {
    var newString = String(string);

    for (var char = 0; char < string.length; char++)
    {
      var result = checkForKeyword(string, char, keywords, "openTag");

      if (result)
      {
        var endTagSplit  = string.substr(char).split(result.closeTag); //explode($result["closeTag"], );
        var innerContent = endTagSplit[0];

        if (result.closeTag == "}")
        {
          var number = 1;
          while (innerContent.split("{").length > number)
          {
            innerContent = innerContent + "}" + endTagSplit[number++];
          }
        }

        var outerContent = result.openTag + innerContent + result.closeTag;

        var handlerArguments = {
          "keyword": result,
          "content": {
            // "outer" => substr($fileContent, char-strlen($result["openTag"]), strlen($result["openTag"])) . $innerContent . substr($fileContent, $char+strlen($innerContent), strlen($result["closeTag"])),
            "outer": outerContent,
            "inner": innerContent
          }
        };

        if (result.handler)
        {
          var newOuterContent = result.handler(innerContent, handlerArguments);
          newString = newString.replace(outerContent, newOuterContent);
        }
      }
    }
    return newString;
  }

  function checkForKeyword(string, position, needles, property)
  {
    for (var i = 0; i < needles.length; i++)
    {
      var query = needles[i];
      var needle = query[property];
      var slice = string.substr(position - needle.length, needle.length);

      // if (string.substr(position - needle.length, position).indexOf(needle) > -1)
      if (slice == needle)
        return query;
    }

    return false;
  }
}

})();
