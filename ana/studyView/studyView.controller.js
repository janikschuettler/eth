/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {

"use strict";

angular
  .module("app")
  .controller("StudyViewController", StudyViewController);

StudyViewController.$inject = ["$scope", "dataService", "hotkeys", "settings", "$timeout", "$routeParams"];

function StudyViewController($scope, dataService, hotkeys, settings, $timeout, $routeParams) {

  var self = $scope;

  var index = 0;
  var studyQueue = [];

  var timeout;
  var studiedCards = {};

  var defaultSettings = {
    randomOrder: true,
    skipStudied: false,
    skipIrrelevantForTest: true,
    studyChapter: "", // "All"
    studyType: "", // "All"
    catalogOnly: true
  };

  self.index = 0;
  self.queueLength = 0;
  self.showProgress = false;

  self.currentData = {};
  self.showResult = false;

  self.settings = angular.copy(settings.study) || defaultSettings;



  // func
  self.showNextItem = showNextItem;
  self.showPreviousItem = showPreviousItem;
  self.toggleView = toggleView;

  activate();

  ////////
  Object.defineProperties(self, {});

  hotkeys.bindTo($scope)
    .add({
      combo: "left",
      callback: showPreviousItem
    });

  hotkeys.bindTo($scope)
    .add({
      combo: "right",
      callback: showNextItem
    });

  hotkeys.bindTo($scope)
    .add({
      combo: ["space", "up", "down"], // TODO
      description: 'blah blah',
      callback: toggleView
    });

  self.$on("databaseFinishedLoading", function() {
    startQueue();
  });

  $scope.$on('$routeChangeSuccess', function() {
    var convertToType = function(type, value) {
      value = typeof type === "boolean" && value == "false" ? false : value;
      return (type.constructor)(value);
    }
    for (var key in self.settings)
      self.settings[key] = $routeParams[key] ? convertToType(self.settings[key], $routeParams[key]) : self.settings[key];
  });

  ////////
  function activate()
  {
    if (dataService.isLoaded)
      startQueue();
    
    if (localStorage.studiedCards)
      studiedCards = JSON.parse(localStorage.studiedCards);

    settings.study = settings.study ? settings.study : defaultSettings;
  }

  function startQueue()
  {
    index = -1;
    self.index = 0;

    var data = {};

    // request theorems and definitions filtered by chapter in case of a chapter filter
    if (self.settings.studyChapter) {
      data = dataService.getSortedTheoremsAndDefinitionsWithinChapter(self.settings.studyChapter, self.settings.skipIrrelevantForTest);
    }
    else {
      data = dataService.getSortedTheoremsAndDefinitions(self.settings.skipIrrelevantForTest);
    }

    var newData = [];

    data.forEach(function(element, index) 
    {
      var studyCardObject = {
        questionBody: element.name || element.summary,
        answerBody: element.tex
      };

      if (element.isDefinition)
      {
        if (self.settings.studyType == "theorem")
          return;

        studyCardObject.questionTitle = (element.definesObject.length > 0) ? "Wie sind folgende Begriffe definiert?" : "Wie ist folgender Begriff definiert?";
        studyCardObject.answerTitle = "Definition " + element.lecture_note_id;
        studyCardObject.questionList = element.definesObject;

        newData.push(studyCardObject);
      }

      if (element.isTheorem) 
      {
        if (self.settings.studyType == "def")
          return;
        
        var theoremObject = dataService.getTheoremObject(element.id);

        studyCardObject.questionTitle = "Was besagt folgender Satz?";
        var answerTitle  = theoremObject.theorem.type + " " + element.lecture_note_id;
            answerTitle += element.name ? " - " + element.name + " " : " ";
            // answerTitle += "(Kapitel " + theoremObject.chapter.number + ")";
        studyCardObject.answerTitle = answerTitle;
      
        newData.push(studyCardObject);

        return;

        // functionality for studying proofs, currently disabled
        if (theoremObject.proofs[0].short) 
        {
          var studyCardObject2 = {};
          studyCardObject2.questionTitle = "Wie wird folgender Satz bewiesen?";
          studyCardObject2.questionBody  = element.name || element.summary;
          studyCardObject2.answerBody    = theoremObject.proofs[0].short;
          studyCardObject2.answerTitle   = "Beweis " + answerTitle;

          newData.push(studyCardObject2);
        }
      }
    });

    // randomize the data
    if (self.settings.randomOrder) {
      studyQueue = dataService.randomize(newData);
    }
    else {
      studyQueue = newData;
    }

    // studyQueue.forEach(function(element) {
    //   studiedCards[element.id] = 0;
    // });

    self.queueLength = studyQueue.length;

    console.log("Starting queue. Length of queue: " + studyQueue.length);

    if (studyQueue.length < 1) {
      self.queueIsEmpty = true;
      return;
    }
    else {
      self.queueIsEmpty = false;
      showNextItem();
    }
  }

  function showNextItem()
  {

    if (index > studyQueue.length - 2) {
      index = -1;
    }

    self.showResult = false;
    self.currentData = studyQueue[++index];

    // if (self.settings.skipStudied) {
    //   if (timeout) $timeout.cancel(timeout);
    //   timeout = $timeout(markCurrentCardAsStudied, 5000);
    // }

    // if (self.currentData.isTheorem && !self.currentData.name && !self.currentData.summary) {
    //   console.log("Omitting theorem because of lack of meta data");
    //   showNextItem();
    // }
    // else if (self.settings.skipStudied && checkIfCardHasbeenStudied(self.currentData)) {
    //   console.log("Skipping item because it has already been studied");
    //   showNextItem();
    // }
    // else if ((self.settings.studyType == "def" && self.currentData.isTheorem) ||
    //          (self.settings.studyType == "theorem" && self.currentData.isDefinition))
    // {
    //   showNextItem();
    // }
    // else {
      self.index = index + 1;
    // }
  }

  function showPreviousItem()
  {
    if (index < 1) {
      index = studyQueue.length;
    }

    self.showResult = false;
    self.currentData = studyQueue[--index];

    // if (self.settings.skipStudied) {
    //   if (timeout) $timeout.cancel(timeout);
    //   timeout = $timeout(markCurrentCardAsStudied, 5000);
    // }

    // if (self.currentData.isTheorem && !self.currentData.name) {
    //   showPreviousItem();
    // }
    // else if ((self.settings.studyType == "def" && self.currentData.isTheorem) ||
    //          (self.settings.studyType == "theorem" && self.currentData.isDefinition))
    // {
    //   showPreviousItem();
    // }
    // else {
      self.index = index + 1; 
    // }
  }

  function markCurrentCardAsStudied()
  {
    studiedCards[self.currentData.id] = studiedCards[self.currentData.id] ? studiedCards[self.currentData.id]++ : 1;
    localStorage.studiedCards = JSON.stringify(studiedCards);
  }

  function checkIfCardHasbeenStudied(card)
  {
    return studiedCards[card.id] && studiedCards[card.id] > 0;
  }

  function toggleView()
  {
    self.showResult = self.showResult ? false : true;
  }
}


function createLatexFileFromCatalog(data)
{
  self.catalog = "";
  data.forEach(function(dataElement) {
    var object = dataService.getTheoremObject(dataElement.id);
    // console.log(object);

    var typeConverter = {
      "1": "Satz",
      "2": "Proposition",
      "3": "Lemma",
      "4": "Kollolar"
    };

    var type = typeConverter[object.theorem.type_id];

    self.catalog += "\\begin{namedtheorem}{" + type + " " + object.theorem.lecture_note_id + "}[" + object.theorem.name + "]";
    self.catalog += "\\label{" + object.theorem.latex_label + "} \n";
    self.catalog += object.theorem.tex + "\n";
    self.catalog += "\\end{namedtheorem} \n"
    // self.catalog += "\\begin{proof} \n" + object.proofs[0].tex + "\n\\end{proof}\n\n";
    // self.catalog += "\\noindent\\rule{3cm}{0.4pt} \n\n";
    self.catalog += "\n\n";
    // self.catalog += "\\cleardoublepage";
  });
}


})();

