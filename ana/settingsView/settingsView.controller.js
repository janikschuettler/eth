/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

(function() {
"use strict";

angular
  .module("app")
  .controller("SettingsViewController", SettingsViewController);

SettingsViewController.$inject = ["$scope", "settings", "dataService"];

function SettingsViewController($scope, settings, dataService) 
{
  var self = $scope;
  self.title = "SettingsView";

  var defaultSettings = {
    randomOrder: true,
    skipStudied: false,
    skipIrrelevantForTest: true,
    studyChapter: "",
    studyType: "All"
  };

  self.$watch("userSettings", userSettingsChanged, true);
  self.$on("databaseFinishedLoading", onload);

  self.chapterFilter = chapterFilter;

  self.resetMemory = resetMemory;
  self.resetSettings = settings.clear;
  self.restoreDefaults = restoreDefaults;

  activate();

  ////////////////

  function activate() 
  {
    self.userSettings = settings.study ? settings.study : defaultSettings;

    console.log(settings);
    
    if (dataService.isLoaded)
      self.chapters = dataService.chapters.data;
  }

  function onload()
  {
    self.chapters = dataService.chapters.data;
  }

  function userSettingsChanged(newValue)
  {
    settings.study = {};
    for (var key in newValue)
    {
      settings.study[key] = newValue[key];
    }
  }

  function resetMemory()
  {
    localStorage.removeItem("studiedCards");
  }

  function restoreDefaults()
  {
    userSettingsChanged(defaultSettings);
  }

  function chapterFilter(item) 
  { 
    return item.number.split(".").length < 3; 
  }
}
})();
