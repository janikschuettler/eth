/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

(function() {
"use strict";

angular
    .module("app")
    .service("database", Database);

Database.$inject = ["$rootScope", "$http", "$q"];

/* @ngInject */
function Database($rootScope, $http, $q)
{
  var self = this;

  var tables = {};
      tables.__proto__ = self;

  var backendBaseURL = "https://janiks.me/projects/eth/ana/__/php/api/";

  self.isLoaded = false;

  // func
  self.addTable = addTable;

  activate();

  ////////////////

  function activate()
  {
    var counter = 0;

    // fetch all data in database
    fetchDatabaseTables()
      .then(function(rawTables) {
        rawTables.forEach(function(tableName) 
        {
          sql("SELECT * FROM " + tableName)
            .then(function(result) {
              var data = result.data.result;
              var table = new Table(tableName, data, sql, self);
                  // table.ondelete = deleteRow;
                  // table.onchange = changeEntry;
                  // table.onnew    = addRow;
              tables[tableName] = table;

              if (++counter > rawTables.length - 1)
              {
                self.isLoaded = true;
                $rootScope.$broadcast("databaseFinishedLoading");
              }
            })
            .catch(function(result) {
              console.error("Fetching table data did not work");
              console.log(result);
            });
        });
      });
  }

  function addTable(tableName, newTable) 
  {
    tables[tableName] = newTable;
  }

  function select(data)
  {
    return request("select", data);
  }

  function insert(data)
  {
    return request("insert", data);
  }

  function change(data)
  {
    return request("change", data);
  }

  function deleteR(data)
  {
    return request("delete", data);
  }


  // private
  function request(data, urlAddition)
  {
    var defaultHttpConfig = {
      headers: {
        "Content-Type": undefined
      }
    };

    return $http.post(backendBaseURL + (urlAddition || ""), data, defaultHttpConfig);
  }

  function sql(sql)
  {
    return request({
      sql: sql
    });
  }

  function fetchDatabaseTables()
  {
    return $q(function(resolve, reject) {
      sql("SHOW TABLES")
        .then(function(result) {
          var data = result.data.result.map(function(entry) {
            for (var key in entry)
              return entry[key];
          });
          resolve(data)
        }, reject);
    });
  }

  return tables;
}


function Table(tableName, originalData, sqlHandler, parent)
{
  var self = this;

  var tableName = tableName;
  var columns = Object.keys(originalData[0] || {});
  var idAutoIncrement = 0;

  var data = convertToObjectById(originalData) || {};
      data.__proto__ = self;

  // useful stuff
  self.data    = originalData;
  self.length  = self.data.length;
  self.first   = self.data[0];
  self.last    = self.data[self.data.length - 1];
  self.forEach = self.data.forEach;
  self.addRows = addRows;

  activate();

  ////////////////

  function activate()  {}

  function convertTableRows(array)
  {
    return Array.isArray(array) ? array.map(function(arrayElement) {
      return new TableRow(arrayElement);
    }) : [];
  }

  function convertToObjectById(array)
  {
    var object = {};
    array.forEach(function(arrayElement) {
      object[arrayElement.id] = new TableRow(arrayElement, {
        deleteRow: deleteRow,
        updateEntry: updateEntry
      });
      if (parseInt(arrayElement.id) >= idAutoIncrement)
        idAutoIncrement = parseInt(arrayElement.id) + 1;
    });
    return object;
  }

  function addRows(rowData)
  {
    rowData = Array.isArray(rowData) ? rowData : [rowData];
    var tempIdAutoIncrement = idAutoIncrement;
    var noId = !rowData[0].id;

    if (noId)
    {
      rowData.forEach(function(column) {
        column.id = tempIdAutoIncrement++;
      });
    }

    var localColumns = Object.keys(rowData[0]);

    var sql = "INSERT INTO " + tableName + " (";

    localColumns.forEach(function(column) {
      sql += "`" + column + "`, ";
    });

    sql = sql.slice(0, -2);
    sql += ")";

    sql += " VALUES ";

    rowData.forEach(function(row) 
    {
      sql += "(";
      localColumns.forEach(function(column) {
        sql += "'" + mysql_real_escape_string(row[column]) + "', ";
      });
      sql = sql.slice(0, -2);
      sql += "), ";
    });
    sql = sql.slice(0, -2);
    sql += ";";

    var promise = sqlHandler(sql);

    promise.then(function() {
      idAutoIncrement = tempIdAutoIncrement;
      var newData = convertToObjectById(rowData);
      Object.assign(data, newData);
      self.data = self.data.concat(rowData);
    });

    return promise;
  }

  function deleteRow(rowId)
  {
    var sql = "DELETE FROM `" + tableName + "` WHERE `" + tableName + "`.`id` = " + rowId + ";";
    var premise = sqlHandler(sql);

    premise.then(function() {
      delete data[rowId];
      self.data = self.data.filter(function(element) {
        return element.id != rowId;
      });
    });

    return premise;
  }

  function updateEntry(rowId, columnName, newValue)
  {
    var sql = "UPDATE `" + tableName + "` SET `" + columnName + "` = '" + mysql_real_escape_string(newValue) + "' WHERE `" + tableName + "`.`id` = " + rowId + ";";
    return sqlHandler(sql);
  }

  function mysql_real_escape_string (val) 
  {
    return String(val).replace(/[\0\n\r\b\t\\'"\x1a]/g, function (s) {
      switch (s) {
        case "\0":
          return "\\0";
        case "\n":
          return "\\n";
        case "\r":
          return "\\r";
        case "\b":
          return "\\b";
        case "\t":
          return "\\t";
        case "\x1a":
          return "\\Z";
        case "'":
          return "''";
        case '"':
          return '""';
        default:
          return "\\" + s;
      }
    });
  };

  return data;
}

function TableRow(originalData, handler)
{
  var self = this;

  var data = originalData,
      object    = {};
      object.__proto__ = self;

  self.data = data;

  self.updateValue = updateValue;
  self.delete = deleteRow;

  activate();

  ////////////////

  function activate() 
  {
    convertData();
  }

  function convertData() 
  {
    var addAccessors = function(column) 
    {
      newObject[column] = {
          get: function() {
              return data[column];
          },
          set: function(newValue) {
              updateValue(column, newValue);
          },
          enumerable: true
      };
    };
    var newObject = {};
    for (var column in data) 
    {
      addAccessors(column);
    }
    Object.defineProperties(object, newObject);
  }

  function updateValue(column, newValue)
  {
    var promise = handler.updateEntry(data.id, column, newValue)
    promise.then(function() {
      data[column] = newValue;
    });
    return promise;

  }

  function deleteRow()
  {
    return handler.deleteRow(data.id);
  }

  return object;
}
})();   
 