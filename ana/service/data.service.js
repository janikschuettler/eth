/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

(function() {
"use strict";

angular
    .module("app")
    .service("dataService", DataService);

DataService.$inject = ["$rootScope", "database", "$q", "latexConfig", "$sce"];

/* @ngInject */
function DataService($rootScope, database, $q, latexConfig, $sce)
{
  var self = this;

  var database = database;
      database.__proto__.__proto__ = self;

  var theoremObjects = {};

  self.lectureBaseUrl = "http://www.video.ethz.ch/etc/designs/mmp/paella/video.html";

  self.latexLabels = [];
  self.lectureNoteIds = [];

  // func
  self.getSortedTheoremsAndDefinitions = getSortedTheoremsAndDefinitions;
  self.getRandomizedTheoremsAndDefinitions = getRandomizedTheoremsAndDefinitions;
  self.getTheoremsAndDefinitionsByChapters = getTheoremsAndDefinitionsByChapters;
  self.getSortedTheoremsAndDefinitionsWithinChapter = getSortedTheoremsAndDefinitionsWithinChapter;
  self.filterOutCatalog = filterOutCatalog;

  self.getTheoremObject = getTheoremObject;
  self.setTheoremObject = setTheoremObject;

  self.randomize = shuffleArray;
  self.renderLectureUrl = renderLectureUrl;

  activate();

  ////////////////

  // initiators
  function activate() {}

  $rootScope.$on("databaseFinishedLoading", onDatabaseFinishedLoading);

  ///////////////
  function onDatabaseFinishedLoading() 
  {
    // console.log("Database finished");
    console.log(database);

    self.latexLabels = [];
    self.lectureNoteIds = [];

    database.theorems.forEach(function(theorem) {
      if (theorem.latex_label) {
        self.latexLabels.push(theorem.latex_label);
        latexConfig.refLabels[theorem.latex_label] = theorem;
      }
      if (theorem.lecture_note_id)
        self.lectureNoteIds.push(theorem.lecture_note_id);
    });

    database.definitions.forEach(function(definition) {
      definition.definesObject = JSON.parse(definition.defines || "[]");
    });
  }

  // base functions
  function getSortedTheoremsAndDefinitions()
  {
    var sortedTheoremsAndDefinitions = [];

    var counter = {
      definitions: 1,
      theorems: 1
    };

    var firstComesFirst = function(first, second)
    {
      if (counter.theorems >= theorems.data.length)
        return false;

      if (counter.definitions >= definitions.data.length)
        return true;

      var firstChapterSplit  = first.lecture_note_id.split(".");
      var secondChapterSplit = second.lecture_note_id.split(".");

      if (parseInt(firstChapterSplit[0]) > parseInt(secondChapterSplit[0]))
        return false;

      else if (parseInt(firstChapterSplit[0]) < parseInt(secondChapterSplit[0]))
        return true;

      if (parseInt(firstChapterSplit[1]) >= parseInt(secondChapterSplit[1]))
        return false;

      else if (parseInt(firstChapterSplit[1]) < parseInt(secondChapterSplit[1]))
        return true;

      return true;
    };

    var theorems = database.theorems;
    var definitions = database.definitions;

    while (true)
    {
      if (counter.theorems >= theorems.data.length && 
          counter.definitions >= definitions.data.length)
      {
        break;
      }

      var currentIsTheorem = firstComesFirst(theorems[counter.theorems], definitions[counter.definitions]);
      var current = currentIsTheorem ? theorems[counter.theorems] : definitions[counter.definitions];

      if (currentIsTheorem)
        current.isTheorem = true;
      else {
        current.isDefinition = true;
        current.type_id = 6;
      }

      currentIsTheorem ? counter.theorems++ : counter.definitions++;

      sortedTheoremsAndDefinitions.push(current);
    }

    return sortedTheoremsAndDefinitions;
  }

  function getRandomizedTheoremsAndDefinitions()
  {
    var definitions = database.definitions.data.map(function(element) {
      element.isDefinition = true;
      return element;
    });
    var theorems = database.theorems.data.map(function(element) {
      element.isTheorem = true;
      return element;
    });

    var definitionsAndTheorems = Math.random() > 0.5 ? definitions.concat(theorems) : theorems.concat(definitions);

    return shuffleArray(shuffleArray(shuffleArray(definitionsAndTheorems)));
  }

  function getSortedTheoremsAndDefinitionsWithinChapter(mainChapter)
  {
    var data = getSortedTheoremsAndDefinitions();

    var mainChapterSplit = mainChapter.split(".");

    var chapterIsWithinMainChapter = function(chapter)
    {
      var chapterSplit = chapter.split(".");
      for (var i = 0; i < mainChapterSplit.length; i++)
        if (mainChapterSplit[i] != chapterSplit[i])
          return false
      return true;
    };

    return data.filter(function(element) {
      return chapterIsWithinMainChapter(database.chapters[element.chapter_id].number);
    });
  }

  function getTheoremsAndDefinitionsByChapters()
  {
    var theoremsAndDefinitionsByChapter = {
      number: "",
      content: []
    };

    var sorted = getSortedTheoremsAndDefinitions();

    var theorems = database.theorems;
    var definitions = database.definitions;

    sorted.forEach(function(element) {

      var chapterSplit = database.chapters[element.chapter_id].number.split(".");
      var chapterObject = theoremsAndDefinitionsByChapter;

      chapterSplit.forEach(function(chapterName, index)
      {
        var lastChapterObject = chapterObject.content.last();
        if (chapterObject.content.length < 1 || lastChapterObject.isTheorem || lastChapterObject.isDefinition || lastChapterObject.number.split(".").last() != chapterName)
        {
          var newChapterObject = {
            name: "", // TODO chapter name lookup
            number: chapterObject.number ? chapterObject.number + "." + chapterName : chapterName,
            content: []
          };

          chapterObject.content.push(newChapterObject);
        }

        chapterObject = chapterObject.content[chapterObject.content.length - 1];
      });

      chapterObject.content.push(element);
    });

    return theoremsAndDefinitionsByChapter.content;
  }

  function getTheoremObject(theoremId)
  {
    var theoremObject = {};

    var copy = function(object) {
      // return JSON.parse(JSON.stringify(object));
      return JSON.parse(JSON.stringify(object.data));
    };

    theoremObject.theorem = copy(database.theorems[theoremId]);

    theoremObject.chapter = database.chapters[theoremObject.theorem.chapter_id];
    theoremObject.lecture = database.lectures[theoremObject.theorem.lecture_id];
    theoremObject.type    = database.types_of_theorems[theoremObject.theorem.type_id]

    theoremObject.theorem.type = database.types_of_theorems[theoremObject.theorem.type_id].type;
    theoremObject.theorem.lectureUrl = renderLectureUrl(theoremId);

    // proofs
    theoremObject.proofs = [];
    theoremObject.linkedTheorems = [];
    theoremObject.linkedDefinitions = [];

    for (var i = 0; i < database.theorems_to_proofs.data.length; i++)
    {
      var link = database.theorems_to_proofs.data[i];
      if (link.theorem_id == theoremId) {
        theoremObject.proofs.push(copy(database.proofs[link.proof_id]));

        var linkedTheorems = database.proofs_to_theorems.data.filter(function(element) {
          return element.proof_id == link.proof_id;
        }).map(function(element) {
          var object = database.theorems[element.theorem_id];
              object.linkId = element.id;
          return object;
        });

        var linkedDefinitions = database.proofs_to_definitions.data.filter(function(element) {
          return element.proof_id == link.proof_id;
        }).map(function(element) {
          var object = database.definitions[element.definition_id];
              object.linkId = element.id;
          return object;
        });

        theoremObject.linkedTheorems = theoremObject.linkedTheorems.concat(linkedTheorems);
        theoremObject.linkedDefinitions = theoremObject.linkedDefinitions.concat(linkedDefinitions);
      }
    }

    // premises and statements
    theoremObject.premises = [];
    theoremObject.statements = [];
    theoremObject.premises_to_statements = [];

    // notes
    theoremObject.notes = [];

    theoremObjects[theoremId] = theoremObject;

    return theoremObject;
  }

  function filterOutCatalog(data)
  {
    data = data.filter(function(dataElement) {
      return dataElement.is_in_catalog == "1";
    });
    return data;
  }

  function setTheoremObject(theoremObject)
  {
    var id = theoremObject.theorem.id || theoremObject.id;
    var oldTheoremObject = theoremObjects[id] ? theoremObjects[id] : getTheoremObject(id);
    var proofId = oldTheoremObject.proofs[0].id;

    var promises = [];

    var updateValue = function(oldObject, newObject) 
    {
      for (var value in oldObject)
      {
        if (oldObject.hasOwnProperty(value) && oldObject[value] != newObject[value] && newObject[value])
          promises.push(oldObject.updateValue(value, newObject[value]));
          // oldObject[value] = newObject[value];
      }
    };

    updateValue(database.theorems[id], theoremObject.theorem);
    updateValue(database.proofs[theoremObject.proofs[0].id], theoremObject.proofs[0]);

    var deleteOrAdd = function(oldObject, newObject, isTheorem) 
    {
      var oldObjectIsLonger = oldObject.length > newObject.length;
      var length = oldObjectIsLonger ? oldObject.length : newObject.length;

      var createIdList = function(array)
      {
        return array ? array.map(function(element) {
          return element.id;
        }) : [];
      }

      var ids = oldObjectIsLonger ? createIdList(newObject) : createIdList(oldObject);

      var create = [];

      for (var i = 0; i < length; i++)
      {
        var element = oldObjectIsLonger ? oldObject[i] : newObject[i];
        if (ids.indexOf(element.id) < 0)
        {
          if (oldObjectIsLonger)
          {
            if (isTheorem)
              promises.push(database.proofs_to_theorems[element.linkId].delete());
            else
              promises.push(database.proofs_to_definitions[element.linkId].delete());
          }
          else
          {
            var createObject = {};
            createObject.proof_id = proofId;
            createObject[isTheorem ? "theorem_id" : "definition_id"] = element.id;

            create.push(createObject);
          }
        }
      }

      if (create.length > 0)
      {
        if (isTheorem)
          promises.push(database.proofs_to_theorems.addRows(create));
        else
          promises.push(database.proofs_to_definitions.addRows(create));
      }
    };

    deleteOrAdd(oldTheoremObject.linkedTheorems, theoremObject.linkedTheorems, true);
    deleteOrAdd(oldTheoremObject.linkedDefinitions, theoremObject.linkedDefinitions, false);

    var allPremises = $q.all(promises);

    allPremises.then(onDatabaseFinishedLoading);

    return allPremises;
  }

  function renderLectureUrl(theoremId, videoPosition)
  {
    var theorem = database.theorems[theoremId],
        lecture = database.lectures[theorem.lecture_id],
        videoPosition = videoPosition || theorem.video_position;

    if (!lecture) return;

    return $sce.trustAsResourceUrl(self.lectureBaseUrl + "?id=" + lecture.video_id + "&time=" + videoPosition);
  }

  // helper
  function shuffleArray(array) 
  {
    if (!array || array.length < 1)
      return array;
    
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) 
    {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  return database;

}
})();  