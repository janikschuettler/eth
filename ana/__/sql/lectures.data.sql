/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('1', '2016-09-21', 'a65b10a4-c135-3bca-a4f5-c37d9f8b0913');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('2', '2016-09-22', 'f3ace887-652a-383b-8035-06b24c58bf96');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('3', '2016-09-26', 'f9dbff54-8594-35ca-bd35-acfebacfd2a6');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('4', '2016-09-28', '13038b7e-fca2-3704-a78e-1278ca58b1b4');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('5', '2016-09-29', '5bf21b4c-0212-3b87-bb45-417ab2e7f4dc');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('6', '2016-10-03', 'edfedcab-4502-35f4-b544-f8d5ef5d01ec');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('7', '2016-10-05', 'b5308ffb-ca26-35a3-a953-6792f9662276');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('8', '2016-10-06', '5c05d7d5-d6e8-3cdd-829c-23c631f02a05');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('9', '2016-10-10', '5220c5a6-a4d7-33f5-9a8a-12cf7ff55a50');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('10', '2016-10-12', 'b8f65259-7596-3d86-bb68-cc95063a554f');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('11', '2016-10-13', '2ee5a831-af2b-31ef-8900-7d3a112952ff');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('12', '2016-10-19', '74716db8-5ead-3bc4-bda2-6ffb78510d45');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('13', '2016-10-20', '3dd27228-39be-36a5-9e09-8d0c2d78ad3a');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('14', '2016-10-24', 'c0e36c8c-7978-354a-bccf-4ef3791ea06d');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('15', '2016-10-26', '580c7545-c328-326d-a5e6-0385710182cb');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('16', '2016-10-26', '2c985c1a-ac93-358e-b99c-459dbc0784da');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('17', '2016-10-27', 'b307b199-153e-328e-8c92-b15e337c1f13');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('18', '2016-10-31', '2308cf51-2882-3053-bdb9-390f72eafbee');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('19', '2016-11-02', 'c1748e62-2b64-30ec-b4c0-96f675f4b5a5');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('20', '2016-11-03', '296d5191-acac-3af7-a84c-a2b179fb9eb3');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('21', '2016-11-07', '9aceecf5-f8f2-3005-bfb1-855b635ad250');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('22', '2016-11-10', '1c0fd9f7-73bb-3030-a61c-340883feda52');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('23', '2016-11-10', 'f4096ce4-951e-47bd-ac08-94c8c6c0500e');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('24', '2016-11-14', '5a4eb02b-509d-3237-8388-3686d1f822f2');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('25', '2016-11-16', '6d899b02-8927-32b8-8e7f-f140c6ee2699');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('26', '2016-11-17', '432dedac-7ad8-349b-87b2-f66e8724e97c');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('27', '2016-11-21', '80f79bc0-3625-3952-8410-7dcc7ca8960a');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('28', '2016-11-23', 'bde06507-10c6-3478-9bde-5e8275ef4a92');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('29', '2016-11-24', '9224687d-141d-397a-a02a-041ee58904f8');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('30', '2016-11-28', '694407a6-f79c-30c9-9bcd-f72b80ed775f');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('31', '2016-11-30', '362d566b-505b-3c11-bf78-d3be0a7f4268');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('32', '2016-12-01', '23ffe085-0344-3b94-b17b-5f0a5efa0f9f');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('33', '2016-12-05', '2a327b17-4b48-3c02-80be-a8f321400f2f');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('34', '2016-12-07', '0142f884-8b4a-3a04-9217-75f7f7c7ef5c');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('35', '2016-12-08', 'aad5365f-19a0-3eeb-885b-9aef151dfaaa');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('36', '2016-12-14', 'ccb7aa42-e49f-3dcd-b30a-d268d027c896');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('37', '2016-12-15', '3af9dfe1-057d-35c2-a842-44282394680d');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('38', '2016-12-19', 'f035d0ed-2cca-3ca0-9f34-dd7f73ecfbb7');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('39', '2016-12-21', '05183157-90f0-3bf7-aeee-a39f58bf7f6c');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('40', '2016-12-22', '610a028f-f399-36b6-8091-64559abcb123');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('41', '2017-02-20', 'eddea48b-a247-4918-92ea-ceb6ad3f5a1e');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('42', '2017-02-22', '33a7408b-f52f-3952-b602-d30aea3b4ee3');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('43', '2017-02-23', 'f98e5a4b-757a-3cd7-a804-9de2935ec261');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('44', '2017-02-27', '488dd792-bd3f-3666-a995-832ab780a0a7');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('45', '2017-03-01', '35956e3c-7739-39f0-bb71-caddc85fde8c');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('46', '2017-03-02', 'ebc8321b-648b-3cba-8148-a1998e8efa33');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('47', '2017-03-06', '82ec4aca-c2fd-3b51-a10e-de0b986a5e0c');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('48', '2017-03-08', 'e97697f4-522f-3f0e-b83b-cf191c6cd177');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('49', '2017-03-09', 'a349286e-33a5-36ba-921e-1b70d0674464');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('50', '2017-03-13', '719a3b72-19b9-340b-827d-8950fc4489db');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('51', '2017-03-15', '5a0d4a87-c40c-3072-bb90-f5b0808850d6');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('52', '2017-03-16', '82be3461-1805-318a-bc11-bd74098420f6');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('53', '2017-03-20', 'd10a28e6-2962-36cf-9839-cfc72db1cf84');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('54', '2017-03-22', '526f0a56-4d72-3ee0-9f88-512cb7d1fb7d');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('55', '2017-03-23', '8bed2bd7-cb76-37db-a42b-b37ebfeadf09');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('56', '2017-03-27', '7a122756-ecae-3dd2-8906-075ed8c89fd8');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('57', '2017-03-29', 'ed6057b5-e89f-3e15-b9f8-3ed6011754bb');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('58', '2017-03-30', 'a2d2b760-164f-34be-9d5b-d8dbd088a45f');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('59', '2017-04-03', 'ea60007f-6511-36e5-9dea-a92bc94b1e35');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('60', '2017-04-05', '86427e21-0e30-3f72-96c0-8eb3465efe62');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('61', '2017-04-06', 'aa86e580-6031-324a-b45c-414298c593d7');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('62', '2017-04-10', 'be009a20-251a-3968-a498-388f2c503639');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('63', '2017-04-12', '14b53d23-f7bb-3b36-a7f6-5d2f1e379551');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('64', '2017-04-13', 'efdc4fe9-4b8f-3600-a856-bd0b1e8d4acb');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('65', '2017-04-24', '1849ebfd-6c8b-36f6-b2bb-8d2077e454b2');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('66', '2017-04-26', '2f2d1825-32cc-3544-9c91-8dbb87b95617');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('67', '2017-04-27', '9edffd76-efe6-3b1d-8c6e-5e98eb8384ca');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('68', '2017-05-03', '5a87e216-80a1-34f9-81d5-78a1f8ab750b');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('69', '2017-05-04', '9d927421-c923-3e88-95f9-0f46d037349f');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('70', '2017-05-08', '09156320-e124-3e5c-9dfd-e5d80a126733');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('71', '2017-05-10', '68f23b59-72b9-3723-96de-9ad1c27fe736');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('72', '2017-05-11', '7881fe9a-8a69-3e26-acfd-3eabc43bbc67');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('73', '2017-05-15', '3ff730cc-f629-4740-88ab-582f2046a078');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('74', '2017-05-17', '1be6b519-bf7c-3c88-801b-2570adcf762c');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('75', '2017-05-18', 'abf952f7-b150-3cc7-ad4f-ad2092b4e753');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('76', '2017-05-22', '162cdbd0-cd74-3f0d-9068-fb872662fea0');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('77', '2017-05-24', 'f3bb6aae-1865-3cab-9074-bb787c06397e');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('78', '2017-05-29', 'eed27f09-8874-3ab3-9f30-8502e62ecbac');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('79', '2017-05-31', 'e585f864-cf32-369b-b8c8-db45005be6c6');
INSERT INTO `lectures` (`id`, `lecture_date`, `video_id`) VALUES ('80', '2017-06-01', '394cb7c4-cb28-3446-a1b8-a8313c23e7ca');
