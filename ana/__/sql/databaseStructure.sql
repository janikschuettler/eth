/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

 -- generated on dbdesigner.net
 

CREATE TABLE `types_of_theorems` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `lectures` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `lecture_date` DATE NOT NULL,
  `video_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `theorems` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `lecture_note_id` varchar(10) NOT NULL,
  `latex_label` varchar(200) NOT NULL,
  `name` varchar(500) NOT NULL,
  `type_id` smallint NOT NULL,
  `summary` varchar(1000) NOT NULL,
  `chapter_id` smallint(10) NOT NULL,
  `importance` smallint(4) NOT NULL,
  `video_position` varchar(8) NOT NULL,
  `lecture_id` smallint NOT NULL,
  `tex` TEXT NOT NULL,
  `premises` TEXT NOT NULL,
  `statements` TEXT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `theorems_to_proofs` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `theorem_id` smallint NOT NULL,
  `proof_id` smallint NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `proofs_to_theorems` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `proof_id` smallint NOT NULL,
  `theorem_id` smallint NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `premises_to_statements` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `premise_pattern_id` smallint NOT NULL,
  `statement_pattern_id` smallint NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `patterns` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `tex` varchar(5000) NOT NULL,
  `short` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `theorems_to_premises_to_statements` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `theorem_id` smallint NOT NULL,
  `premises_to_statements_id` smallint NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `definitions` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `lecture_note_id` varchar(10) NOT NULL,
  `latex_label` varchar(200) NOT NULL,
  `name` varchar(500) NOT NULL,
  `summary` varchar(1000) NOT NULL,
  `chapter_id` smallint(10) NOT NULL,
  `importance` smallint(4) NOT NULL,
  `video_position` varchar(8) NOT NULL,
  `lecture_id` smallint NOT NULL,
  `defines` TEXT NOT NULL,
  `tex` TEXT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `proofs` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `tex` TEXT NOT NULL,
  `short` TEXT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `proofs_to_definitions` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `proof_id` smallint NOT NULL,
  `definition_id` smallint NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `chapters` (
  `id` smallint NOT NULL AUTO_INCREMENT,
  `number` varchar(10) NOT NULL,
  `title` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `theorems` ADD CONSTRAINT `theorems_fk0` FOREIGN KEY (`type_id`) REFERENCES `types_of_theorems`(`id`);

ALTER TABLE `theorems` ADD CONSTRAINT `theorems_fk1` FOREIGN KEY (`chapter_id`) REFERENCES `chapters`(`id`);

ALTER TABLE `theorems` ADD CONSTRAINT `theorems_fk2` FOREIGN KEY (`lecture_id`) REFERENCES `lectures`(`id`);

ALTER TABLE `theorems_to_proofs` ADD CONSTRAINT `theorems_to_proofs_fk0` FOREIGN KEY (`theorem_id`) REFERENCES `theorems`(`id`);

ALTER TABLE `theorems_to_proofs` ADD CONSTRAINT `theorems_to_proofs_fk1` FOREIGN KEY (`proof_id`) REFERENCES `proofs`(`id`);

ALTER TABLE `proofs_to_theorems` ADD CONSTRAINT `proofs_to_theorems_fk0` FOREIGN KEY (`proof_id`) REFERENCES `proofs`(`id`);

ALTER TABLE `proofs_to_theorems` ADD CONSTRAINT `proofs_to_theorems_fk1` FOREIGN KEY (`theorem_id`) REFERENCES `theorems`(`id`);

ALTER TABLE `premises_to_statements` ADD CONSTRAINT `premises_to_statements_fk0` FOREIGN KEY (`premise_pattern_id`) REFERENCES `patterns`(`id`);

ALTER TABLE `premises_to_statements` ADD CONSTRAINT `premises_to_statements_fk1` FOREIGN KEY (`statement_pattern_id`) REFERENCES `patterns`(`id`);

ALTER TABLE `theorems_to_premises_to_statements` ADD CONSTRAINT `theorems_to_premises_to_statements_fk0` FOREIGN KEY (`theorem_id`) REFERENCES `theorems`(`id`);

ALTER TABLE `theorems_to_premises_to_statements` ADD CONSTRAINT `theorems_to_premises_to_statements_fk1` FOREIGN KEY (`premises_to_statements_id`) REFERENCES `premises_to_statements`(`id`);

ALTER TABLE `definitions` ADD CONSTRAINT `definitions_fk0` FOREIGN KEY (`chapter_id`) REFERENCES `chapters`(`id`);

ALTER TABLE `definitions` ADD CONSTRAINT `definitions_fk1` FOREIGN KEY (`lecture_id`) REFERENCES `lectures`(`id`);

ALTER TABLE `proofs_to_definitions` ADD CONSTRAINT `proofs_to_definitions_fk0` FOREIGN KEY (`proof_id`) REFERENCES `proofs`(`id`);

ALTER TABLE `proofs_to_definitions` ADD CONSTRAINT `proofs_to_definitions_fk1` FOREIGN KEY (`definition_id`) REFERENCES `definitions`(`id`);
