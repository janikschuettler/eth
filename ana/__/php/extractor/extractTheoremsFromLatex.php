<?php
/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

require_once "extractorInclude.php";

/*
  id
  lecture_note_id
  latex_label
  name 
  type
  summary
  lecture_note_chapter
  importance 
  video_position
  lecture_id
  tex
  premises
  statements
*/

$parser = new Parser($order);
$theorems = $parser->parse();

$theorem_columns = [
  "id",
  "lecture_note_id",
  "latex_label",
  "name",
  "type",
  "summary",
  "lecture_note_chapter",
  "importance",
  "video_position",
  "lecture_id",
  "tex",
  "premises",
  "statements"
];

$proof_columns = [
  "id",
  "tex",
  "short"
];

$theorem_proof_columns = [
  "id",
  "theorem_id",
  "proofs_id"
];

$sql_theorem = "INSERT INTO `theorems` (`id`, `lecture_note_id`, `latex_label`, `name`, `type`, `summary`, `lecture_note_chapter`, `importance`, `video_position`, `lecture_id`, `tex`, `premises`, `statements`) VALUES \n";

echo htmlspecialchars($sql_theorem) . "<br>";

foreach ($theorems as $theorem) 
{
  // jslog($theorem["theorem"]["id"]);
  // theorem
  $new = "(";
  $sql_theorem .= "(";
  foreach ($theorem_columns as $column) {
    // jslog(" " . "'" . $mysql->real_escape_string($theorem["theorem"][$column]) . "', ");
    $sql_theorem .= "'" . $mysql->real_escape_string($theorem["theorem"][$column]) . "', "; 
    $new .= "'" . $mysql->real_escape_string($theorem["theorem"][$column]) . "', "; 
  }
  $sql_theorem = substr($sql_theorem, 0, -2);
  $sql_theorem .= "), \n";
  $new = substr($new, 0, -2);
  $new .= "),";

  echo htmlspecialchars($new) . "<br>";
}

$sql_proof = "INSERT INTO `proofs` (`id`, `tex`, `short`) VALUES \n";
echo htmlspecialchars($sql_proof) . "<br>";

foreach ($theorems as $theorem) 
{
  // proof
  $new = "(";
  $sql_proof .= "(";
  foreach ($proof_columns as $column) {
    $new .= "'" . $mysql->real_escape_string($theorem["proof"][$column]) . "', ";
    $sql_proof .= "'" . $mysql->real_escape_string($theorem["proof"][$column]) . "', ";
  }
  $sql_proof = substr($sql_proof, 0, -2);
  $sql_proof .= "), \n";
  $new = substr($new, 0, -2);
  $new .= "),";

  echo htmlspecialchars($new) . "<br>";
}

$theorem_proof_id_counter = 1;
$sql_theorem_proof = "INSERT INTO `theorems_to_proofs` (`id`, `theorem_id`, `proofs_id`) VALUES \n";

foreach ($theorems as $theorem) 
{
  // theorem_to_proof
  $sql_theorem_proof .= "(";
  $sql_theorem_proof .= "'" . $theorem_proof_id_counter++ . "', ";
  $sql_theorem_proof .= "'" . $mysql->real_escape_string($theorem["theorem"]["id"]) . "', ";
  $sql_theorem_proof .= "'" . $mysql->real_escape_string($theorem["proof"]["id"]) . "'";
  $sql_theorem_proof .= "), \n";
}

jslog($sql_theorem_proof);

// $sql_theorem       = substr($sql_theorem, 0, -3);
// $sql_proof         = substr($sql_proof, 0, -3);
// $sql_theorem_proof = substr($sql_theorem_proof, 0, -3);

// $sql = $sql_theorem . " \n\n " . $sql_proof . " \n\n " . $sql_theorem_proof;

// echo "<pre>" . $sql . "</pre>";

// jslog(json_encode($theorems, JSON_PRETTY_PRINT));
jslog($theorems);     




/**
 * Parser
 */
class Parser
{
  var $path        = "";
  var $fileOrder   = [];
  var $keywords    = [];

  var $content = [];
  var $theorems = [];
  var $context = [
    "id" => 1,
    "proof_id" => 1,
    "chapter" => 2,
    "section" => 0,
    "subsection" => 0,
    "subsubsection" => 0,
    "lecture_id" => 1,
    "lecture_note_id_counter" => 1,
    "lecture_date" => "2016-09-29"
  ];

  // helper var
  var $theoremLabel = "";
  var $proofs = [];


  // func
  function Parser($fileOrder)
  {
    $this->path = dirname(dirname(dirname(__FILE__))) . "/latex/latest/Sections/";
    $this->fileOrder = $fileOrder;

    $this->keywords = [
      [
        "openTag" => "\\chapter{",
        "closeTag" => "}",
        "handler" => "handleKeywordChapter"
      ], [
        "openTag" => "\\section{",
        "closeTag" => "}",
        "handler" => "handleKeywordSection"
      ], [
        "openTag" => "\\subsection{",
        "closeTag" => "}",
        "handler" => "handleKeywordSubsection"
      ], [
        "openTag" => "\\subsubsection{",
        "closeTag" => "}",
        "handler" => "handleKeywordSubsubsection"
      ], [
        "openTag" => "\\lecture{",
        "closeTag" => "}",
        "handler" => "handleLectureKeyword"
      ], [
        "openTag" => "\\begin{proof}",
        "closeTag" => "\\end{proof}",
        "handler" => "handleKeywordProof"
      ], [
        "openTag" => "\\begin{satz}",
        "closeTag" => "\\end{satz}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "satz");
        }
      ], [
        "openTag" => "\\begin{theorem}",
        "closeTag" => "\\end{theorem}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "satz");
        }
      ], [
        "openTag" => "\\begin{namedtheorem}",
        "closeTag" => "\\end{namedtheorem}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "namedtheorem");
        }
      ], [
        "openTag" => "\\begin{proposition}",
        "closeTag" => "\\end{proposition}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "proposition");
        }
      ], [
        "openTag" => "\\begin{lemma}",
        "closeTag" => "\\end{lemma}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "lemma");
        }
      ], [
        "openTag" => "\\begin{corollary}",
        "closeTag" => "\\end{corollary}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "korollar");
        }
      ], /*[
        "openTag" => "\n%",
        "closeTag" => "\n",
        "handler" => "handleComments"
      ], */
      ["openTag" => "\\begin{remark}", "closeTag" => "\\end{remark}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{exercise}", "closeTag" => "\\end{exercise}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{essexercise}", "closeTag" => "\\end{essexercise}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{definition}", "closeTag" => "\\end{definition}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{example}", "closeTag" => "\\end{example}", "handler" => "increaseLectureIdCounter"],
    ];
  }

  function parse()
  {
    foreach ($this->fileOrder as $file)
    {
      $fileContent = file_get_contents($this->path . $file);
      $this->loopThroughText($fileContent, $this->keywords);
    }

    return $this->theorems;
  }

  function loopThroughText($string, $keywords)
  {
    for ($char = 0; $char < strlen($string); $char++)
    {
      $result = $this->checkForKeyword($string, $char, $keywords, "openTag");

      if ($result)
      {

        $endTagSplit  = explode($result["closeTag"], substr($string, $char));
        $innerContent = $endTagSplit[0];

        if ($result["closeTag"] == "}")
        {
          $number = 1;
          while (sizeof(explode("{", $innerContent)) > $number)
          {
            $innerContent = $innerContent . "}" . $endTagSplit[$number++];
          }
        }

        $handlerArgument = [
          "keyword" => $result,
          "content" => [
            // "outer" => substr($fileContent, $char-strlen($result["openTag"]), strlen($result["openTag"])) . $innerContent . substr($fileContent, $char+strlen($innerContent), strlen($result["closeTag"])),
            "outer" => $result["openTag"] . $innerContent . $result["closeTag"],
            "inner" => $innerContent
          ]
        ];
        if ($this->checkIfLineIsCommented($string, $char)) 
        {
          // jslog("Skipping line because line is commented out");
          // jslog($handlerArgument["content"]);
          continue;
        }

        // jslog($this->context);

        if (!is_string($result["handler"]))
        {
          $result["handler"]($handlerArgument);
        }
        else
        {
          call_user_func([$this, $result["handler"]], $handlerArgument);
        }
      }
    }
  }

  function checkIfLineIsCommented($string, $position)
  {
    $linePosition = $position;
    while ($linePosition-- > $position - 100) {
      if (substr($string, $linePosition, 1) == "\n") {
        break;
      }
    }

    if (substr($string, $linePosition+1, 1) == "%")
    {
      // jslog("Found comment");
      // jslog(substr($string, $linePosition+1, $position - $linePosition+1)); 
      return true;
    }
    return false;
  }

  function checkForKeyword($string, $position, $needles, $property, $offset=0)
  {
    foreach($needles as $query)
    {
      $needle = $query[$property];
      if(strpos(substr($string, $position - strlen($needle), strlen($needle)), $needle, $offset) !== false) 
        return $query;
    }
    return false;
  }

  // handler
  function handlekeywordChapter($result)
  {
    $this->context["chapter"]++;
    $this->context["section"] = 0;
    $this->context["subsection"] = 0;
    $this->context["subsubsection"] = 0;
    // $this->context["lecture_note_id_counter"] = 1;
    $this->addChapter($result);
  }

  function handleKeywordSection($result)
  {
    $this->context["section"]++;
    $this->context["subsection"] = 0;
    $this->context["subsubsection"] = 0;
  }

  function handleKeywordSubsection($result)
  {
    $this->context["subsection"]++;
    $this->context["subsubsection"] = 0;
  }

  function handleKeywordSubsubsection($result)
  {
    $this->context["subsubsection"]++;
  }

  function handleLectureKeyword($result)
  {
    $monthData = [
      "Sep" => "09",
      "Okt" => "10",
      "Nov" => "11",
      "Dez" => "12",
      "Feb" => "02",
      "Mär" => "03",
      "März" => "03",
      "Apr" => "04",
      "April" => "04",
      "Mai" => "05",
      "Jun" => "06"
    ];

    $rawContent = $result["content"]["inner"];
    $uglyDate = trim(str_replace(".", "", $rawContent));
    $uglyDate = explode(",", $uglyDate)[0];
    $uglyDate = trim(explode("VO", $uglyDate)[1]);
    
    if (strlen($uglyDate) < 3 || strrpos($uglyDate, "~") > -1)
      return;

    $day = preg_replace("/[^0-9,.]/", "", $uglyDate);
    $day = (strlen($day) < 2) ? "0" . $day : $day;

    $uglyDate = trim(substr($uglyDate, 0, 8));
    $monthRaw = explode(" ", $uglyDate)[1];
    $monthRaw = $monthRaw ? $monthRaw : substr($uglyDate, -3);
    $month = isset($monthData[$monthRaw]) ? $monthData[$monthRaw] : "";
    $year = intval($month) > 8 ? "2016" : "2017";
    $date = $year . "-" . $month . "-" . $day;

    if (!$month)
      return;
    
    $lectureDate = $date;
    $lectureId = $this->lectureIdentifierCallback($lectureDate);

    if ($lectureId) {
      $this->context["lecture_date"] = $lectureDate;
      $this->context["lecture_id"] = $lectureId + 1;
    }
  }

  function handleTheoremKeyword($result, $type)
  {
    $tex = trim($result["content"]["inner"]);
    $this->loopThroughText($tex, [[ 
      "openTag" => "\\label{",
      "closeTag" => "}",
      "handler" => function($subresult) {
        if (!$this->theoremLabel)
          $this->theoremLabel = $subresult["content"]["inner"];
      }
    ]]);

    // extract theorem name
    $name = $tex[0] == "[" ? substr(explode("]", $tex)[0], 1) : "";
    $nameLength = $name ? strlen($name) + 2 : 0;
    $tex = substr($tex, $nameLength);
    $tex = str_replace("\\label{" . $this->theoremLabel . "}", "", $tex);
    $tex = trim($tex);

    // extract theorem label
    // $labelSplit = explode("\\label", string)
    // $nameLength = $name ? strlen($name) + 2 : 0;
    // $tex = substr($tex, $nameLength);
    // $tex = str_replace("\\label{" . $this->theoremLabel . "}", "", $tex);
    // $tex = trim($tex);

    $splitByDann = explode(", dann", $tex);
    $splitByDann = sizeof($splitByDann) > 1 ? $splitByDann : explode(". Dann", $tex);

    $premises = sizeof($splitByDann) > 1 ? $splitByDann[0] . "." : "";
    $statements = sizeof($splitByDann) > 1 ? "Dann" . $splitByDann[1] : ""; // $splitByDann[0];
    // jslog($splitByDann);

    $typeArray = [
      "satz" => "1",
      "namedtheorem" => "5",
      "proposition" => "2",
      "lemma" => "3",
      "korollar" => "4",
      "axiom" => "5"
    ];

    // actual theorem
    $theorem = [];

    $theorem["id"]              = $this->context["id"]++;
    $theorem["tex"]             = $tex;
    $theorem["statements"]      = $statements;
    $theorem["premises"]        = $premises;
    $theorem["name"]            = $name;
    $theorem["type"]            = $typeArray[$type];
    $theorem["latex_label"]     = $this->theoremLabel;
    $theorem["summary"]         = "";
    $theorem["importance"]      = 1;
    $theorem["lecture_id"]      = $this->context["lecture_id"];
    $theorem["video_position"]  = "";
    $theorem["lecture_note_id"] = $type == "namedtheorem" ? "" : $this->context["chapter"] . "." . $this->context["lecture_note_id_counter"]++;
    $theorem["lecture_note_chapter"] = 
      $this->context["chapter"] .
      ($this->context["section"] > 0 ? "." . $this->context["section"] : "") .
      ($this->context["subsection"] > 0 ? "." . $this->context["subsection"] : "");

    // $theorem["lecture_date"]    = $this->context["lecture_date"];


    $proof = [];

    $proof["id"]    = $this->context["proof_id"]++;
    $proof["tex"]   = ""; // array_slice($proofs, -1)[0];
    $proof["short"] = "";

    $theoremObject = [
      "theorem" => $theorem,
      "proof" => $proof
    ];


    array_push($this->theorems, $theoremObject);

    // jslog($theorem);
    // jslog(json_encode($theoremObject, JSON_PRETTY_PRINT));

    // clean up
    $this->theoremLabel = "";
  }

  function handleKeywordProof($result)
  {
    $proof = trim($result["content"]["inner"]);
    if ($this->theorems[sizeof($this->theorems) - 1]["proof"]["tex"] == "")
    {
      $this->theorems[sizeof($this->theorems) - 1]["proof"]["tex"] = $proof;
    }
    else if ($this->theorems[sizeof($this->theorems) - 2]["proof"]["tex"] == "")
    {
      $this->theorems[sizeof($this->theorems) - 2]["proof"]["tex"] = $proof;
    }
    else
    {
      jslog("Cannot assign proof to theorem: ");
      jslog($proof);
      jslog("=========================");
    }
  }

  function increaseLectureIdCounter()
  {
    $this->context["lecture_note_id_counter"]++;
  }

  function handleComments($result)
  {
    // jslog("is this a comment?");
    // jslog($result["content"]["outer"]);
    // jslog("=========================");
  }

  function lectureIdentifierCallback($lectureDate)
  {
    global $lectures;

    if (!isset($lectures[$lectureDate]["id"]))
    {
      jslog("Cannot find find lecture_id for date " . $lectureDate);
      return false;
    }

    return $lectures[$lectureDate]["id"] ? $lectures[$lectureDate]["id"] : -1;
  }

  function addChapter($result)
  {
    $chapterNumber = $this->context["chapter"];

    if ($this->context["section"] > 0)
    {
      $chapterNumber .= "." . $this->context["section"];

      if ($this->context["subsection"] > 0)
      {
        $chapterNumber .= "." . $this->context["subsection"];
      }
    }

    $chapterObject = [
      "id" => $this->context["id"]++,
      "number" => $chapterNumber,
      "title" => $result["content"]["inner"]
    ];

    array_push($this->content, $chapterObject);
  }
}


?>