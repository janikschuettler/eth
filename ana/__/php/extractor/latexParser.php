<?php
/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

include_once "parser.php";

/**
 * Parser
 */
class LatexParser extends Parser
{
  var $context = [
    "id" => [
      "content" => 1,
      "definition" => 1,
      "theorem" => 1,
      "proof" => 1,
      "lecture_note" => 1
    ],
    "chapter" => 0,
    "section" => 0,
    "subsection" => 0,
    "subsubsection" => 0,
    "lecture_id" => 1,
    "lecture_date" => "2016-09-29"
  ];

  var $content = [];
  var $theorems = [];
  var $definitions = [];
  

  // helper var
  var $theoremLabel = "";
  var $proofs = [];

  var $defines = [];
  var $definitionLabel = "";


  // func
  function LatexParser($path, $fileOrder)
  {
    $path = dirname(dirname(dirname(__FILE__))) . "/latex/latest/Sections/";
    $fileOrder = $fileOrder;

    $keywords = [
      [
        "openTag" => "\\chapter{",
        "closeTag" => "}",
        "handler" => "handleKeywordChapter"
      ], [
        "openTag" => "\\section{",
        "closeTag" => "}",
        "handler" => "handleKeywordSection"
      ], [
        "openTag" => "\\section[",
        "closeTag" => "]",
        "handler" => "handleKeywordSection"
      ], [
        "openTag" => "\\subsection{",
        "closeTag" => "}",
        "handler" => "handleKeywordSubsection"
      ], [
        "openTag" => "\\subsubsection{",
        "closeTag" => "}",
        "handler" => "handleKeywordSubsubsection"
      ], [
        "openTag" => "\\lecture{",
        "closeTag" => "}",
        "handler" => "handleLectureKeyword"
      ], 
      
      ["openTag" => "\\begin{remark}", "closeTag" => "\\end{remark}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{exercise}", "closeTag" => "\\end{exercise}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{essexercise}", "closeTag" => "\\end{essexercise}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{example}", "closeTag" => "\\end{example}", "handler" => "increaseLectureIdCounter"],

      [
        "openTag" => "\\begin{definition}",
        "closeTag" => "\\end{definition}",
        "handler" => "handleKeywordDefinition"
      ], 

      [
        "openTag" => "\\begin{proof}",
        "closeTag" => "\\end{proof}",
        "handler" => "handleKeywordProof"
      ], [
        "openTag" => "\\begin{satz}",
        "closeTag" => "\\end{satz}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "satz");
        }
      ], [
        "openTag" => "\\begin{theorem}",
        "closeTag" => "\\end{theorem}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "satz");
        }
      ], [
        "openTag" => "\\begin{namedtheorem}",
        "closeTag" => "\\end{namedtheorem}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "namedtheorem");
        }
      ], [
        "openTag" => "\\begin{proposition}",
        "closeTag" => "\\end{proposition}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "proposition");
        }
      ], [
        "openTag" => "\\begin{lemma}",
        "closeTag" => "\\end{lemma}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "lemma");
        }
      ], [
        "openTag" => "\\begin{corollary}",
        "closeTag" => "\\end{corollary}",
        "handler" => function($result) {
          return $this->handleTheoremKeyword($result, "korollar");
        }
      ],
    ];

    parent::__construct($path, $fileOrder, $keywords);

    $this->parse();
  }

  // handler
  function handlekeywordChapter($result)
  {
    $this->context["chapter"]++;
    $this->context["section"] = 0;
    $this->context["subsection"] = 0;
    $this->context["subsubsection"] = 0;
    $this->context["id"]["lecture_note"] = 1;
    $this->addChapter($result);
  }

  function handleKeywordSection($result)
  {
    $this->context["section"]++;
    $this->context["subsection"] = 0;
    $this->context["subsubsection"] = 0;
    $this->addChapter($result);
  }

  function handleKeywordSubsection($result)
  {
    $this->context["subsection"]++;
    $this->context["subsubsection"] = 0;
    $this->addChapter($result);
  }

  function handleKeywordSubsubsection($result)
  {
    $this->context["subsubsection"]++;
    $this->addChapter($result);
  }

  function handleLectureKeyword($result)
  {
    $monthData = [
      "Sep" => "09",
      "Okt" => "10",
      "Nov" => "11",
      "Dez" => "12",
      "Feb" => "02",
      "Mär" => "03",
      "März" => "03",
      "Apr" => "04",
      "April" => "04",
      "Mai" => "05",
      "Jun" => "06"
    ];

    $rawContent = $result["content"]["inner"];
    $uglyDate = trim(str_replace(".", "", $rawContent));
    $uglyDate = explode(",", $uglyDate)[0];
    $uglyDate = trim(explode("VO", $uglyDate)[1]);
    
    if (strlen($uglyDate) < 3 || strrpos($uglyDate, "~") > -1)
      return;

    $day = preg_replace("/[^0-9,.]/", "", $uglyDate);
    $day = (strlen($day) < 2) ? "0" . $day : $day;

    $uglyDate = trim(substr($uglyDate, 0, 8));
    $monthRaw = explode(" ", $uglyDate)[1];
    $monthRaw = $monthRaw ? $monthRaw : substr($uglyDate, -3);
    $month = isset($monthData[$monthRaw]) ? $monthData[$monthRaw] : "";
    $year = intval($month) > 8 ? "2016" : "2017";
    $date = $year . "-" . $month . "-" . $day;

    if (!$month)
      return;
    
    $lectureDate = $date;
    $lectureId = $this->lectureIdentifierCallback($lectureDate);

    if ($lectureId) {
      $this->context["lecture_date"] = $lectureDate;
      $this->context["lecture_id"] = $lectureId + 1;
    }
  }

  function handleKeywordDefinition($result) 
  {
    $tex = trim($result["content"]["inner"]);
    $this->loopThroughText($tex, [[ 
      "openTag" => "\\textbf{",
      "closeTag" => "}",
      "handler" => function($subresult) {
        array_push($this->defines, $subresult["content"]["inner"]);
      }
    ], [ 
      "openTag" => "\\label{def",
      "closeTag" => "}",
      "handler" => function($subresult) {
        $this->definitionLabel = "def" . $subresult["content"]["inner"];
      }
    ]]);

    $name = $tex[0] == "[" ? substr(explode("]", $tex)[0], 1) : "";
    $nameLength = $name ? strlen($name) + 2 : 0;
    $tex = substr($tex, $nameLength);
    $tex = str_replace("\\label{" . $this->definitionLabel . "}", "", $tex);
    $tex = trim($tex);

    // actual definition
    $definition = [];

    $definition["id"]              = $this->context["id"]["definition"]++;
    $definition["tex"]             = $tex;
    $definition["name"]            = $name;
    $definition["label"]           = $this->definitionLabel;
    $definition["summary"]         = "";
    $definition["defines"]         = json_encode($this->defines);
    $definition["importance"]      = 1;
    $definition["chapter_id"]      = $this->context["id"]["content"];
    $definition["lecture_id"]      = $this->context["lecture_id"];
    $definition["video_position"]  = "";
    $definition["lecture_note_id"] = $this->context["chapter"] . "." . $this->context["id"]["lecture_note"]++;

    array_push($this->definitions, $definition);

    // clean up
    $this->defines = [];
    $this->definitionLabel = "";
  }

  function handleTheoremKeyword($result, $type)
  {
    $tex = trim($result["content"]["inner"]);
    $this->loopThroughText($tex, [[ 
      "openTag" => "\\label{",
      "closeTag" => "}",
      "handler" => function($subresult) {
        if (!$this->theoremLabel)
          $this->theoremLabel = $subresult["content"]["inner"];
      }
    ]]);

    $nameInSquare = "";
    $nameInCurly = "";

    // extract theorem name
    $nameInSquare = $tex[0] == "[" ? substr(explode("]", $tex)[0], 1) : "";
    $nameLength = $nameInSquare ? strlen($nameInSquare) + 2 : 0;
    $tex = substr($tex, $nameLength);
    $tex = str_replace("\\label{" . $this->theoremLabel . "}", "", $tex);

    if ($tex[0] == "{") {
      $nameInCurly = substr(explode("}", $tex)[0], 1);
      $nameLength = $nameInCurly ? strlen($nameInCurly) + 2 : 0;
      $tex = substr($tex, $nameLength);
      $tex = str_replace("{" . $nameInCurly . "}", "", $tex);
    }

    $tex = trim($tex);

    $name = $nameInSquare ? $nameInSquare : $nameInCurly;

    // extract theorem label
    // $labelSplit = explode("\\label", string)
    // $nameLength = $name ? strlen($name) + 2 : 0;
    // $tex = substr($tex, $nameLength);
    // $tex = str_replace("\\label{" . $this->theoremLabel . "}", "", $tex);
    // $tex = trim($tex);

    $splitByDann = explode(", dann", $tex);
    $splitByDann = sizeof($splitByDann) > 1 ? $splitByDann : explode(". Dann", $tex);

    $premises = sizeof($splitByDann) > 1 ? $splitByDann[0] . "." : "";
    $statements = sizeof($splitByDann) > 1 ? "Dann" . $splitByDann[1] : ""; // $splitByDann[0];
    // jslog($splitByDann);

    $typeArray = [
      "satz" => "1",
      "namedtheorem" => "5",
      "proposition" => "2",
      "lemma" => "3",
      "korollar" => "4",
      "axiom" => "5"
    ];

    // actual theorem
    $theorem = [];

    $theorem["id"]              = $this->context["id"]["theorem"]++;
    $theorem["tex"]             = $tex;
    $theorem["statements"]      = $statements;
    $theorem["premises"]        = $premises;
    $theorem["name"]            = $name;
    $theorem["type_id"]         = $typeArray[$type];
    $theorem["latex_label"]     = $this->theoremLabel;
    $theorem["chapter_id"]      = $this->context["id"]["content"];
    $theorem["summary"]         = "";
    $theorem["importance"]      = 1;
    $theorem["lecture_id"]      = $this->context["lecture_id"];
    $theorem["video_position"]  = "";
    $theorem["lecture_note_id"] = $type == "namedtheorem" ? "" : $this->context["chapter"] . "." . $this->context["id"]["lecture_note"]++;


    $proof = [];
    
    $proof["id"]    = $this->context["id"]["proof"]++;
    $proof["tex"]   = ""; // array_slice($proofs, -1)[0];
    $proof["short"] = "";

    $theoremObject = [
      "theorem" => $theorem,
      "proof" => $proof
    ];


    array_push($this->theorems, $theoremObject);

    // clean up
    $this->theoremLabel = "";
  }

  function handleKeywordProof($result)
  {
    $proof = trim($result["content"]["inner"]);
    if ($this->theorems[sizeof($this->theorems) - 1]["proof"]["tex"] == "")
    {
      $this->theorems[sizeof($this->theorems) - 1]["proof"]["tex"] = $proof;
    }
    else if ($this->theorems[sizeof($this->theorems) - 2]["proof"]["tex"] == "")
    {
      $this->theorems[sizeof($this->theorems) - 2]["proof"]["tex"] = $proof;
    }
    else
    {
      jslog("Cannot assign proof to theorem: ");
      jslog($proof);
      jslog("=========================");
    }
  }

  function increaseLectureIdCounter()
  {
    $this->context["id"]["lecture_note"]++;
  }

  function handleComments($result)
  {
    // jslog("is this a comment?");
    // jslog($result["content"]["outer"]);
    // jslog("=========================");
  }

  function lectureIdentifierCallback($lectureDate)
  {
    global $lectures;

    if (!isset($lectures[$lectureDate]["id"]))
    {
      jslog("Cannot find find lecture_id for date " . $lectureDate);
      return false;
    }

    return $lectures[$lectureDate]["id"] ? $lectures[$lectureDate]["id"] : -1;
  }

  function addChapter($result)
  {
    $chapterNumber = $this->context["chapter"];

    if ($this->context["section"] > 0)
    {
      $chapterNumber .= "." . $this->context["section"];

      if ($this->context["subsection"] > 0)
      {
        $chapterNumber .= "." . $this->context["subsection"];
      }
    }

    $chapterObject = [
      "id" => $this->context["id"]["content"]++,
      "number" => $chapterNumber,
      "title" => $result["content"]["inner"]
    ];

    array_push($this->content, $chapterObject);
  }

  function getCurrentChapter()
  {
    return $this->context["chapter"] .
      ($this->context["section"] > 0 ? "." . $this->context["section"] : "") .
      ($this->context["subsection"] > 0 ? "." . $this->context["subsection"] : "");
  }
}

?>