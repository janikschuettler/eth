<?php 
/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

/**
 * Parser
 */
class Parser
{
  var $path        = "";
  var $fileOrder   = [];
  var $keywords    = [];

  // func
  function __construct($path, $fileOrder, $keywords)
  {
    $this->path = $path;
    $this->fileOrder = $fileOrder;

    $this->keywords = $keywords;
  }

  function parse()
  {
    foreach ($this->fileOrder as $file)
    {
      $fileContent = file_get_contents($this->path . $file);
      $this->loopThroughText($fileContent, $this->keywords);
    }
  }

  function loopThroughText($string, $keywords)
  {
    for ($char = 0; $char < strlen($string); $char++)
    {
      $result = $this->checkForKeyword($string, $char, $keywords, "openTag");

      if ($result)
      {
        $endTagSplit  = explode($result["closeTag"], substr($string, $char));
        $innerContent = $endTagSplit[0];

        if ($result["closeTag"] == "}")
        {
          $number = 1;
          while (sizeof(explode("{", $innerContent)) > $number)
          {
            $innerContent = $innerContent . "}" . $endTagSplit[$number++];
          }
        }

        $handlerArgument = [
          "keyword" => $result,
          "content" => [
            // "outer" => substr($fileContent, $char-strlen($result["openTag"]), strlen($result["openTag"])) . $innerContent . substr($fileContent, $char+strlen($innerContent), strlen($result["closeTag"])),
            "outer" => $result["openTag"] . $innerContent . $result["closeTag"],
            "inner" => $innerContent
          ]
        ];
        if ($this->checkIfLineIsCommented($string, $char)) 
        {
          // jslog("Skipping line because line is commented out");
          // jslog($handlerArgument["content"]);
          continue;
        }

        // jslog($this->context);

        if (!is_string($result["handler"]))
        {
          $result["handler"]($handlerArgument);
        }
        else
        {
          call_user_func([$this, $result["handler"]], $handlerArgument);
        }
      }
    }
  }

  function checkIfLineIsCommented($string, $position)
  {
    $linePosition = $position;
    while ($linePosition-- > $position - 100) {
      if (substr($string, $linePosition, 1) == "\n") {
        break;
      }
    }

    if (substr($string, $linePosition+1, 1) == "%")
    {
      // jslog("Found comment");
      // jslog(substr($string, $linePosition+1, $position - $linePosition+1)); 
      return true;
    }
    return false;
  }

  function checkForKeyword($string, $position, $needles, $property, $offset=0)
  {
    foreach($needles as $query)
    {
      $needle = $query[$property];
      if(strpos(substr($string, $position - strlen($needle), strlen($needle)), $needle, $offset) !== false) 
        return $query;
    }
    return false;
  }
}


?>