<?php
/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

require_once "extractorInclude.php";

/*
definition:
  id
  latex_label
  name
  lecture_note_chapter
  lecture_id
  tex
  defines
  [lecture_note_id]
  [summary]
  [importance]
  [video_position]
*/

$parser = new Parser($order);
$definitions = $parser->parse();

$sql = "INSERT INTO `definitions` (`id`, `latex_label`, `name`, `lecture_note_chapter`, `lecture_id`, `tex`, `defines`, `lecture_note_id`, `summary`, `importance`, `video_position`) \n VALUES ";

foreach ($definitions as $definition) 
{
  $sql .= "(";
  $sql .= "'" . $mysql->real_escape_string($definition["id"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["latex_label"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["name"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["lecture_note_chapter"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["lecture_id"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["tex"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["defines"]) . "', ";
  $sql .= "'" . $mysql->real_escape_string($definition["lecture_note_id"]) . "', ";
  $sql .= "'" . "" . "', ";
  $sql .= "'" . "1" . "', ";
  $sql .= "'" . "" . "'";
  $sql .= "), \n";
}

$sql = substr($sql, 0, -3);

jslog($sql);

jslog(json_encode($definitions, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));




/**
 * Parser
 */
class Parser
{
  var $path        = "";
  var $fileOrder   = [];
  var $keywords    = [];

  var $definitions = [];
  var $context = [
    "id" => 1,
    "chapter" => 2,
    "section" => 0,
    "subsection" => 0,
    "subsubsection" => 0,
    "lecture_id" => 1,
    "lecture_note_id_counter" => 1,
    "lecture_date" => "2016-09-29"
  ];

  // helper var
  var $defines = [];
  var $definitionLabel = "";


  // func
  function Parser($fileOrder)
  {
    $this->path = dirname(dirname(dirname(__FILE__))) . "/latex/latest/Sections/";
    $this->fileOrder = $fileOrder;

    $this->keywords = [
      [
        "openTag" => "\\section{",
        "closeTag" => "}",
        "handler" => "handleKeywordSection"
      ], [
        "openTag" => "\\subsection{",
        "closeTag" => "}",
        "handler" => "handleKeywordSubsection"
      ], [
        "openTag" => "\\subsubsection{",
        "closeTag" => "}",
        "handler" => "handleKeywordSubsubsection"
      ], [
        "openTag" => "\\begin{definition}",
        "closeTag" => "\\end{definition}",
        "handler" => "handleKeywordDefinition"
      ], [
        "openTag" => "\\lecture{",
        "closeTag" => "}",
        "handler" => "handleLectureKeyword"
      ],
      ["openTag" => "\\begin{remark}", "closeTag" => "\\end{remark}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{exercise}", "closeTag" => "\\end{exercise}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{essexercise}", "closeTag" => "\\end{essexercise}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{example}", "closeTag" => "\\end{example}", "handler" => "increaseLectureIdCounter"],

      ["openTag" => "\\begin{proposition}", "closeTag" => "\\end{proposition}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{lemma}", "closeTag" => "\\end{lemma}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{corollary}", "closeTag" => "\\end{corollary}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{satz}", "closeTag" => "\\end{satz}", "handler" => "increaseLectureIdCounter"],
      ["openTag" => "\\begin{theorem}", "closeTag" => "\\end{theorem}", "handler" => "increaseLectureIdCounter"],
    ];
  }

  function parse()
  {
    foreach ($this->fileOrder as $file)
    {
      $fileContent = file_get_contents($this->path . $file);
      $currentChapter = explode(" - ", $file)[0];

      if ($this->context["chapter"] != $currentChapter)
      {
        $this->context["chapter"] = $currentChapter;
        $this->context["section"] = 0;
        $this->context["subsection"] = 0;
        $this->context["subsubsection"] = 0;
        $this->context["lecture_note_id_counter"] = 1;
      }

      $this->loopThroughText($fileContent, $this->keywords);
    }

    return $this->definitions;
  }

  function loopThroughText($string, $keywords)
  {
    for ($char = 0; $char < strlen($string); $char++)
    {
      $result = $this->checkForKeyword($string, $char, $keywords, "openTag");

      if ($result)
      {

        $endTagSplit  = explode($result["closeTag"], substr($string, $char));
        $innerContent = $endTagSplit[0];

        if ($result["closeTag"] == "}")
        {
          $number = 1;
          while (sizeof(explode("{", $innerContent)) > $number)
          {
            $innerContent = $innerContent . "}" . $endTagSplit[$number++];
          }
        }

        $handlerArgument = [
          "keyword" => $result,
          "content" => [
            // "outer" => substr($fileContent, $char-strlen($result["openTag"]), strlen($result["openTag"])) . $innerContent . substr($fileContent, $char+strlen($innerContent), strlen($result["closeTag"])),
            "outer" => $result["openTag"] . $innerContent . $result["closeTag"],
            "inner" => $innerContent
          ]
        ];
        if ($this->checkIfLineIsCommented($string, $char)) 
        {
          // jslog("Skipping line because line is commented out");
          // jslog($handlerArgument["content"]);
          continue;
        }

        // jslog($this->context);

        if (!is_string($result["handler"]))
        {
          $result["handler"]($handlerArgument);
        }
        else
        {
          call_user_func([$this, $result["handler"]], $handlerArgument);
        }
      }
    }
  }

  function checkIfLineIsCommented($string, $position)
  {
    $linePosition = $position;
    while ($linePosition-- > $position - 100) {
      if (substr($string, $linePosition, 1) == "\n") {
        break;
      }
    }

    if (substr($string, $linePosition+1, 1) == "%")
    {
      // jslog("Found comment");
      // jslog(substr($string, $linePosition+1, $position - $linePosition+1)); 
      return true;
    }
    return false;
  }

  function checkForKeyword($string, $position, $needles, $property, $offset=0)
  {
    foreach($needles as $query)
    {
      $needle = $query[$property];
      if(strpos(substr($string, $position - strlen($needle), strlen($needle)), $needle, $offset) !== false) 
        return $query;
    }
    return false;
  }

  // handler
  function handleKeywordSection($result)
  {
    $this->context["section"]++;
    $this->context["subsection"] = 0;
    $this->context["subsubsection"] = 0;
  }

  function handleKeywordSubsection($result)
  {
    $this->context["subsection"]++;
    $this->context["subsubsection"] = 0;
  }

  function handleKeywordSubsubsection($result)
  {
    $this->context["subsubsection"]++;
  }

  function handleLectureKeyword($result)
  {
    $monthData = [
      "Sep" => "09",
      "Okt" => "10",
      "Nov" => "11",
      "Dez" => "12",
      "Feb" => "02",
      "Mär" => "03",
      "März" => "03",
      "Apr" => "04",
      "April" => "04",
      "Mai" => "05",
      "Jun" => "06"
    ];

    $rawContent = $result["content"]["inner"];
    $uglyDate = trim(str_replace(".", "", $rawContent));
    $uglyDate = explode(",", $uglyDate)[0];
    $uglyDate = trim(explode("VO", $uglyDate)[1]);
    
    if (strlen($uglyDate) < 3 || strrpos($uglyDate, "~") > -1)
      return;

    $day = preg_replace("/[^0-9,.]/", "", $uglyDate);
    $day = (strlen($day) < 2) ? "0" . $day : $day;

    $uglyDate = trim(substr($uglyDate, 0, 8));
    $monthRaw = explode(" ", $uglyDate)[1];
    $monthRaw = $monthRaw ? $monthRaw : substr($uglyDate, -3);
    $month = isset($monthData[$monthRaw]) ? $monthData[$monthRaw] : "";
    $year = intval($month) > 8 ? "2016" : "2017";
    $date = $year . "-" . $month . "-" . $day;

    if (!$month)
      return;
    
    $lectureDate = $date;
    $lectureId = $this->lectureIdentifierCallback($lectureDate);

    if ($lectureId) {
      $this->context["lecture_date"] = $lectureDate;
      $this->context["lecture_id"] = $lectureId + 1;
    }
  }

  function handleKeywordDefinition($result) 
  {
    $tex = trim($result["content"]["inner"]);
    $this->loopThroughText($tex, [[ 
      "openTag" => "\\textbf{",
      "closeTag" => "}",
      "handler" => function($subresult) {
        array_push($this->defines, $subresult["content"]["inner"]);
      }
    ], [ 
      "openTag" => "\\label{def",
      "closeTag" => "}",
      "handler" => function($subresult) {
        $this->definitionLabel = "def" . $subresult["content"]["inner"];
      }
    ]]);

    $name = $tex[0] == "[" ? substr(explode("]", $tex)[0], 1) : "";
    $nameLength = $name ? strlen($name) + 2 : 0;
    $tex = substr($tex, $nameLength);
    $tex = str_replace("\\label{" . $this->definitionLabel . "}", "", $tex);
    $tex = trim($tex);

    // actual definition
    $definition = [];

    jslog($tex);

    $definition["id"]              = $this->context["id"]++;
    $definition["tex"]             = $tex;
    $definition["name"]            = $name;
    $definition["label"]           = $this->definitionLabel;
    $definition["summary"]         = "";
    $definition["defines"]         = json_encode($this->defines); //implode(", ", $this->defines);
    $definition["importance"]      = 1;
    $definition["lecture_id"]      = $this->context["lecture_id"];
    $definition["video_position"]  = "";
    $definition["lecture_note_id"] = $this->context["chapter"] . "." . $this->context["lecture_note_id_counter"]++;
    $definition["lecture_note_chapter"] = 
      $this->context["chapter"] .
      ($this->context["section"] > 0 ? "." . $this->context["section"] : "") .
      ($this->context["subsection"] > 0 ? "." . $this->context["subsection"] : "");

    // $definition["lecture_date"]    = $this->context["lecture_date"];

    array_push($this->definitions, $definition);

    // jslog($definition);

    // clean up
    $this->defines = [];
    $this->definitionLabel = "";
  }

  function lectureIdentifierCallback($lectureDate)
  {
    global $lectures;

    if (!$lectures[$lectureDate]["id"])
    {
      jslog("Cannot find find lecture_id for date " . $lectureDate);
    }

    return $lectures[$lectureDate]["id"] ? $lectures[$lectureDate]["id"] : -1;
  }

  function increaseLectureIdCounter()
  {
    $this->context["lecture_note_id_counter"]++;
  }
}














exit();

$definitions = [];
$currentSection  = "";
$currentLecture  = "29. Sep.";

foreach ($order as $file)
{
  // echo $path . $file . "<br><br>";

  $fileContent      = file_get_contents($path . $file);
  $definitionsSplit = explode("begin{definition}", $fileContent);

  checkForLectureDate($definitionsSplit[0]);

  foreach (array_slice($definitionsSplit, 1) as $definitionSplit) 
  {
    $definition = [];

    $definitionTex = trim(explode("\\end{definition}", $definitionSplit)[0]);
    $latexLabel = "";

    if (substr($definitionTex, 0, 1) == "[") 
    {
      preg_match("/\[(.*?)\]/", $definitionTex, $latexLabel);
      $latexLabel = $latexLabel[1];
      $definitionTex = trim(str_replace("[" . $latexLabel . "]", "", $definitionTex));
    }

    $definition["tex"] = $definitionTex;
    $definition["lecture_date"] = $currentLecture;

    array_push($definitions, $definition);

    checkForLectureDate($definitionSplit);


    // jslog($latexLabel);
    // jslog($definitionTex);
  }
  echo "<br><br>";
}

function checkForLectureDate($input)
{
  global $currentLecture;

  $monthData = [
    "Sep" => "09",
    "Okt" => "10",
    "Nov" => "11",
    "Dez" => "12",
    "Feb" => "02",
    "März" => "03",
    "Apr" => "04",
    "Mai" => "05",
    "Jun" => "06"
  ];

  preg_match("/lecture\{(.*?)\}/", $input, $lecturesSplit);

  if (sizeof($lecturesSplit) > 0)
  {
    $uglyDate = trim(str_replace(".", "", $lecturesSplit[1]));
    $uglyDate = explode(",", $uglyDate)[0];
    $uglyDate = trim(explode("VO", $uglyDate)[1]);
    
    if (strrpos($uglyDate, "~") > -1)
      return;

    $day = preg_replace("/[^0-9,.]/", "", $uglyDate);

    if (strlen($day) < 2) {
      $day = "0" . $day;
    }

    $uglyDate = trim(substr($uglyDate, 0, 6));
    $month = substr($uglyDate, -3);
    $date = "2017-" . $monthData[$month] . "-" . $day;
    
    $currentLecture = $date;
  }
}

echo jslog($definitions);

?>