<?php
/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

// require_once dirname(dirname(__FILE__)) . "/database.php";

$mysql = new MySQLi("127.0.0.1", "root");

if (!$mysql) {
  exit("No Databse connection");
}

function escapeString($string = "")
{
  global $mysql;
  // return $mysql->real_escape_string($string);
  return dontHateMe($string);
}

function escape($value) {
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}

function dontHateMe($unescaped) 
{
  $replacements = array(
     "\x00"=>'\x00',
     "\n"=>'\n',
     "\r"=>'\r',
     "\\"=>'\\\\',
     "'"=>"\'",
     '"'=>'\"',
     "\x1a"=>'\x1a'
  );
  return strtr($unescaped,$replacements);
}

function printAsJsonString($object)
{
  jslog(json_encode($object, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
}

function parseAsSql($tableName, $dataset) 
{
  global $databaseColumns;

  $columns = $databaseColumns[$tableName];

  $sql = "INSERT INTO `" . $tableName . "` ";

  $sql .= "(";
  foreach ($columns as $column) 
  {
    $sql .= "`" . escapeString($column) . "`, ";
  }
  $sql = substr($sql, 0, -2);

  $sql .= ")";
  // $sql .= "(`" . implode("`, `", $columns) . "`)";
  $sql .= "\nVALUES\n";

  foreach ($dataset as $data) 
  {
    $sql .= "(";
    foreach ($columns as $column) 
    {
      $sql .= "'" . escapeString($data[$column]) . "', ";
    }
    $sql = substr($sql, 0, -2);
    $sql .= "), \n";
  }

  $sql = substr($sql, 0, -3);
  $sql .= ";";

  return $sql;
}

$sourcePath = dirname(dirname(dirname(__FILE__))) . "/latex/latest/Sections/";

$databaseColumns = [
  "chapters" => [
    "id",
    "number",
    "title"
  ],
  "definitions" => [
    "id", 
    "lecture_note_id", 
    "latex_label", 
    "chapter_id", 
    "lecture_id", 
    "importance", 
    "summary", 
    "name", 
    "defines", 
    "tex", 
    "video_position"
  ],
  "theorems" => [
    "id",
    "lecture_note_id",
    "latex_label",
    "chapter_id",
    "lecture_id",
    "importance",
    "summary",
    "name",
    "type_id",
    "video_position",
    "tex",
    "premises",
    "statements"
  ],
  "proofs" => [
    "id",
    "tex",
    "short"
  ],
  "theorems_to_proofs" => [
    "id",
    "theorem_id",
    "proof_id"
  ]
];

$sourceOrder = [
  "einfuehrung.tex",
  // // "einfuehrung/flaechenberechnung.tex",
  "einfuehrung/tipps.tex",
  "einfuehrung/logik.tex",
  "einfuehrung/mengen.tex",
  "einfuehrung/zahlenmengen.tex",
  "einfuehrung/beweise.tex",
  
  "real.tex",
  "reelle_zahlen/axiome.tex",
  "reelle_zahlen/natural.tex",
  "reelle_zahlen/komplexe.tex",
  "reelle_zahlen/intervalle.tex",
  "reelle_zahlen/maxundsup.tex",
  "reelle_zahlen/archi-sum-absolut.tex",
  "reelle_zahlen/intervallschachtelung.tex",
  "reelle_zahlen/eindeutigkeit.tex",
  "FktR.tex",
  "elementare_funktionen/summen.tex",
  "elementare_funktionen/polynome.tex",
  "elementare_funktionen/binomial.tex",
  "elementare_funktionen/rwertig.tex",
  "elementare_funktionen/zwsatz.tex",
  "elementare_funktionen/umkabb.tex",
  "riemannintegral.tex",
  "R-integral/treppenfkt.tex",
  "R-integral/defint.tex",
  "R-integral/gesetze.tex",
  "R-integral/monotone.tex",
  "R-integral/teilinterv.tex",
  "R-integral/polynome.tex",
  "folgen.tex",
  "folgen/folgkonv.tex",
  "folgen/rfolg.tex",
  "folgen/exp.tex",
  "folgen/kpt.tex",
  "folgen/fktlim.tex",
  "folgen/rsum.tex",
  "reihen.tex",
  "reihen/reihen.tex",
  "reihen/abskonv.tex",
  "reihen/fktfolg.tex",
  "reihen/potenzreihen.tex",
  "reihen/exp.tex",
  "reihen/trigo.tex",
  "reihen/hypfkt.tex",
  "reihen/intpot.tex",
  "reihen/peano.tex",
  "differentialeindim.tex",
  "differentialrechnung/ableitung.tex",
  "differentialrechnung/wsaetze.tex",
  "differentialrechnung/trigo.tex",
  "differentialrechnung/diffglg.tex",
  "differentialrechnung/hyp.tex",
  "fundamentalsatz.tex",
  "fundsatz/fundsatz.tex",
  "fundsatz/unbestint.tex",
  "fundsatz/bestint.tex",
  "fundsatz/uneig.tex",
  "fundsatz/taylor.tex",
  "fundsatz/numint.tex",
  "fundsatz/asym.tex",
  "metricspace.tex",
  "metrik/metrik.tex",
  "metrik/topo.tex",
  "metrik/stetig.tex",
  "metrik/complete.tex",
  "metrik/compact.tex",
  "metrik/fundalgebra.tex",
  "metrik/real.tex",
  "diffglg.tex",
  "diffglg/diffglg.tex",
  "diffglg/picard.tex",
  "differentialmehrdim.tex",
  "mehrdimdiff/ableitung.tex",
  "mehrdimdiff/chainrule.tex",
  "mehrdimdiff/taylor.tex",
  "mehrdimdiff/extremal.tex",
  "mehrdimdiff/paramint.tex",
  "mehrdimdiff/wegintegrale.tex",
  "mehrdimdiff/konservativ.tex",
  "diffgeo1.tex",
  "diffgeo1/implizit.tex",
  "diffgeo1/submanifold.tex",
  "diffgeo1/extrem.tex",
  "mint.tex",
  "mint/def.tex",
  "mint/lebesguekrit.tex",
  "mint/jordan.tex",
  "mint/fubini.tex",
  "mint/substitution.tex",
  "mint/uneig.tex",
  "divergenzsaetze.tex",
  "divergenzsaetze/green2D.tex",
  "divergenzsaetze/surfaceint.tex",
  "divergenzsaetze/gauss3D.tex",
  "divergenzsaetze/laplace.tex",
  "divergenzsaetze/stokes.tex",
  "divergenzsaetze/partdereins.tex",
  "divergenzsaetze/konservativ.tex",
  "divergenzsaetze/tensor.tex"
];

$lectures = [
  "2017-06-01" => ["id" => 80],
  "2017-05-31" => ["id" => 79],
  "2017-05-29" => ["id" => 78],
  "2017-05-24" => ["id" => 77],
  "2017-05-22" => ["id" => 76],
  "2017-05-18" => ["id" => 75],
  "2017-05-17" => ["id" => 74],
  "2017-05-15" => ["id" => 73],
  "2017-05-11" => ["id" => 72],
  "2017-05-10" => ["id" => 71],
  "2017-05-08" => ["id" => 70],
  "2017-05-04" => ["id" => 69],
  "2017-05-03" => ["id" => 68],
  "2017-04-27" => ["id" => 67],
  "2017-04-26" => ["id" => 66],
  "2017-04-24" => ["id" => 65],
  "2017-04-13" => ["id" => 64],
  "2017-04-12" => ["id" => 63],
  "2017-04-10" => ["id" => 62],
  "2017-04-06" => ["id" => 61], 
  "2017-04-05" => ["id" => 60], 
  "2017-04-03" => ["id" => 59], 
  "2017-03-30" => ["id" => 58], 
  "2017-03-29" => ["id" => 57], 
  "2017-03-27" => ["id" => 56], 
  "2017-03-23" => ["id" => 55], 
  "2017-03-22" => ["id" => 54], 
  "2017-03-20" => ["id" => 53], 
  "2017-03-16" => ["id" => 52], 
  "2017-03-15" => ["id" => 51],
  "2017-03-13" => ["id" => 50],
  "2017-03-09" => ["id" => 49],
  "2017-03-08" => ["id" => 48],
  "2017-03-06" => ["id" => 47],
  "2017-03-02" => ["id" => 46],
  "2017-03-01" => ["id" => 45],
  "2017-02-27" => ["id" => 44],
  "2017-02-23" => ["id" => 43],
  "2017-02-22" => ["id" => 42],
  "2017-02-20" => ["id" => 41],
  "2016-12-22" => ["id" => 40],
  "2016-12-21" => ["id" => 39],
  "2016-12-19" => ["id" => 38],
  "2016-12-15" => ["id" => 37],
  "2016-12-14" => ["id" => 36],
  "2016-12-08" => ["id" => 35],
  "2016-12-07" => ["id" => 34],
  "2016-12-05" => ["id" => 33],
  "2016-12-01" => ["id" => 32],
  "2016-11-30" => ["id" => 31],
  "2016-11-28" => ["id" => 30],
  "2016-11-24" => ["id" => 29],
  "2016-11-23" => ["id" => 28],
  "2016-11-21" => ["id" => 27],
  "2016-11-17" => ["id" => 26],
  "2016-11-16" => ["id" => 25],
  "2016-11-14" => ["id" => 24],
  "2016-11-10" => ["id" => 23],
  "2016-11-10" => ["id" => 22],
  "2016-11-07" => ["id" => 21],
  "2016-11-03" => ["id" => 20],
  "2016-11-02" => ["id" => 19],
  "2016-10-31" => ["id" => 18],
  "2016-10-27" => ["id" => 17],
  "2016-10-26" => ["id" => 16],
  "2016-10-26" => ["id" => 15],
  "2016-10-24" => ["id" => 14],
  "2016-10-20" => ["id" => 13],
  "2016-10-19" => ["id" => 12],
  "2016-10-13" => ["id" => 11],
  "2016-10-12" => ["id" => 10],
  "2016-10-10" => ["id" => 9],
  "2016-10-06" => ["id" => 8],
  "2016-10-05" => ["id" => 7],
  "2016-10-03" => ["id" => 6],
  "2016-09-29" => ["id" => 5],
  "2016-09-28" => ["id" => 4],
  "2016-09-26" => ["id" => 3],
  "2016-09-22" => ["id" => 2],
  "2016-09-21" => ["id" => 1]
];


function jslog($text) {

  if (is_string($text) || is_numeric($text))
    echo "<pre>" . htmlspecialchars($text) . "</pre>";

  else {
    echo "<pre>";
    var_dump($text);
    echo "</pre>";
  }
}

?>