<?php

if (!isset($_GET["mail"]) || !isset($_GET["message"]))
    exit("-1:Missing parameters");

$mail       = urldecode($_GET["mail"]);
$message    = urldecode($_GET["message"]);

if ($mail == "" || $message == "") 
    exit("-1:Empty parameters");

if (!filter_var($mail, FILTER_VALIDATE_EMAIL))
    exit("-1:Invalid mail");

date_default_timezone_set('Europe/Berlin');

$message    = wordwrap($message, 70, "\r\n");
$subject    = "Kontakt-Form Anfrage vom " . strftime('%d.%m.%Y um %H:%M Uhr');
$headers    = 'From: ' . $mail . "\r\n";

$send = mail('info@vvast.de', $subject, $message, $headers);
//$send = mail('trickist17@web.de', $subject, $message, $headers);

if ($send)
    echo "1:Nachricht erfolgreich versendet!";

else
    echo "-1:Fehler beim Versenden der E-Mail";

