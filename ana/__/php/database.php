<?php
/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

require_once "config.php";

$mysql = @new MySQLi($config["database"]["server"], 
                    $config["database"]["user"], 
                    $config["database"]["password"], 
                    $config["database"]["database"]);

if ($mysql->connect_errno) {
  echo makeResponse(false, [
    "error" => "Connection to database failed. PHP says '" . $mysql->connect_error . "'",
    "errorCode" => $mysql->connect_errno
  ]);
  exit();
}

$mysql->query("SET NAMES 'utf8'");

$databaseTables = array_map(function($element) { foreach ($element as $entry) return $entry; }, parseResultAsObject($mysql->query("SHOW TABLES")));


function parseResultAsObject($result)
{
  if (is_bool($result))
    return $result;

  $rows = array();
  
  while($r = $result->fetch_assoc()) {
    $rows[] = $r;
  }

  return $rows;
}

function parseResultAsJson($result)
{
  return json_encode(parseResultAsObject($result));
}

function makeResult($result, $response = [])
{
  global $mysql;

  $response["__check"]      = true;
  $response["result"]       = isset($response["result"])       ? $response["result"]       : parseResultAsObject($result);
  $response["affectedRows"] = isset($response["affectedRows"]) ? $response["affectedRows"] : @$mysql->affected_rows;
  $response["errorCode"]    = isset($response["errorCode"])    ? $response["errorCode"]    : @$mysql->errno;
  $response["errorList"]    = isset($response["errorList"])    ? $response["errorList"]    : @$mysql->error_list;
  $response["error"]        = isset($response["error"])        ? $response["error"]        : $mysql->error;
  $response["info"]         = isset($response["info"])         ? $response["info"]         : @$mysql->info;
  $response["insertId"]     = isset($response["insertId"])     ? $response["insertId"]     : @$mysql->insert_id;

  return $response;
}

function makeResponse($result, $response = [])
{
  return json_encode(makeResult($result, $response));
}

?>