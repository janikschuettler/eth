/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {
"use strict";

angular
  .module("app")
  .service("latexService", LatexService);

LatexService.$inject = [];

function LatexService() 
{
  var self = this;

  var parser = new Parser();

  self.parse = parse;
  self.updateLatex = updateLatex;

  activate();

  ////////////////

  function activate() {}


  function parse(text)
  {
    if (!text)
      return "";

    var keywords = [
      {
        openTag: "\\textbf{",
        closeTag: "}",
        handler: function(text) {return "<b>" + text + "</b>"}
      }, {
        openTag: "\\emph{",
        closeTag: "}",
        handler: function(text) {return "<i>" + text + "</i>"}
      }, {
        openTag: "\\qm{",
        closeTag: "}",
        handler: function(text) {return "\"" + text + "\""}
      }, {
        openTag: "\\begin{itemize}",
        closeTag: "\\end{itemize}",
        handler: listKeywordHandler
      }, {
        openTag: "\\begin{enumerate}",
        closeTag: "\\end{enumerate}",
        handler: listKeywordHandler
      }, {
        openTag: "\\index[symb]{",
        closeTag: "}",
        handler: function() {return ""}
      }, /*{
        openTag: "\\nomenclature{",
        closeTag: "}",
        handler: function() {return ""}
      },*/ {
        openTag: "\\label{",
        closeTag: "}",
        handler: function() {return ""}
      }
    ];

    text = text.replace(/</g, "&lt;");
    text = text.replace(/>/g, "&gt;");
    text = text.replace(/\\enumnewline/g, "");

    /*if (text.substr(0, 7) == "\\label{") {
      bracketSplit = text.split("}");
      text = text.replace(bracketSplit[0] + "}", "");
    }*/

    var parsed = parser.parse(text, keywords);

    parsed = parsed.replace(/\n\s*\n/, "<br>");

    return parsed || text;
  }

  function listKeywordHandler(text, object)
  {
    var newText = "<ul>";

    if (text[0] == "[") 
    {
      var bracketSplit = text.split("]");
      var bracketContent = bracketSplit[0].substr(1);
      text = text.replace("[" + bracketContent + "]", "").trim();
    }

    // TODO remove [(i)] from beginning

    var items = text.split("\\item");
    items.forEach(function(item) {
      item = item.trim();
      if (item)
        newText += "<li>" + item + "</li>";
    });

    newText += "</ul>";

    return newText;
  }


  function parse2(text)
  { 
    if (!text)
      return "";

    var newText;


    // \begin{itemize}
    var listSplit = text.split(/(\\begin{itemize})([\s\S]*?)(\\end{itemize})/gm);

    if (listSplit.length > 1) 
    {
      newText = "";
      var isInsideItemize = false;
      listSplit.forEach(function(part) 
      {
        if (part == "\\begin{itemize}")
        {
          isInsideItemize = true;
          newText += "<ul>"
        }
        else if (part == "\\end{itemize}")
        {
          isInsideItemize = false;
          newText += "</ul>"
        }
        else if (isInsideItemize)
        {
          var items = part.trim().split("\\item");
          items.forEach(function(item) {
            if (item)
              newText += "<li>" + item.trim() + "</li>";
          });
        }
        else
          newText += part;
      });
      text = newText;
    }

    // \begin{enumerate}
    var listSplit = text.split(/(\\begin{enumerate}\[\(i\)\])([\s\S]*?)(\\end{enumerate})/gm);

    if (listSplit.length > 1) 
    {
      newText = "";
      var isInsideItemize = false;
      listSplit.forEach(function(part) 
      {
        if (part == "\\begin{enumerate}[(i)]")
        {
          isInsideItemize = true;
          newText += "<ul>"
        }
        else if (part == "\\end{enumerate}")
        {
          isInsideItemize = false;
          newText += "</ul>"
        }
        else if (isInsideItemize)
        {
          var items = part.trim().split("\\item");
          items.forEach(function(item) {
            if (item)
              newText += "<li>" + item.trim() + "</li>";
          });
        }
        else
          newText += part;
      });
      text = newText;
    }


    // \textbf{}
    var mathbfSplit = text.split(/(\\textbf{)([\s\S]*?)(})/gm);

    if (mathbfSplit.length > 1) 
    {
      newText = "";
      var isInsideItemize = false;
      mathbfSplit.forEach(function(part) 
      {
        if (part == "\\textbf{")
        {
          isInsideItemize = true;
          newText += "<b>";
        }
        else if (part == "}")
        {
          isInsideItemize = false;
          newText += "</b>";
        }
        else
          newText += part;
      });
      text = newText;
    }

    return text;
  }

  function updateLatex()
  {
    try {
      MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
      return true;
    }
    catch (e) {
      return false;
    }
  }
}

function Parser()
{
  var self = this;

  self.keyword = {};

  //
  self.parse = parse;

  ////////////////

  function parse(string, keywords)
  {
    return looper(string, keywords || self.keywords);
  }

  function looper(string, keywords)
  {
    var newString = String(string);

    for (var char = 0; char < string.length; char++)
    {
      var result = checkForKeyword(string, char, keywords, "openTag");

      if (result)
      {
        var endTagSplit  = string.substr(char).split(result.closeTag); //explode($result["closeTag"], );
        var innerContent = endTagSplit[0];

        if (result.closeTag == "}")
        {
          var number = 1;
          while (innerContent.split("{").length > number)
          {
            innerContent = innerContent + "}" + endTagSplit[number++];
          }
        }

        var outerContent = result.openTag + innerContent + result.closeTag;

        var handlerArguments = {
          "keyword": result,
          "content": {
            // "outer" => substr($fileContent, char-strlen($result["openTag"]), strlen($result["openTag"])) . $innerContent . substr($fileContent, $char+strlen($innerContent), strlen($result["closeTag"])),
            "outer": outerContent,
            "inner": innerContent
          }
        };

        if (result.handler)
        {
          var newOuterContent = result.handler(innerContent, handlerArguments);
          newString = newString.replace(outerContent, newOuterContent);
        }
      }
    }
    return newString;
  }

  function checkForKeyword(string, position, needles, property)
  {
    for (var i = 0; i < needles.length; i++)
    {
      var query = needles[i];
      var needle = query[property];
      var slice = string.substr(position - needle.length, needle.length);

      // if (string.substr(position - needle.length, position).indexOf(needle) > -1)
      if (slice == needle)
        return query;
    }

    return false;

    /*foreach($needles as $query)
    {
      $needle = $query[$property];
      if(strpos(substr($string, $position - strlen($needle), strlen($needle)), $needle, $offset) !== false) 
        return $query;
    }
    return false;*/
  }
}

})();
