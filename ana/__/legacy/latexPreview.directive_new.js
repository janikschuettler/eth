/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {

"use strict";

angular.module("latex", [])
  .directive('latex', ["latexService", function (latexService) {
    return {
      scope: {
        bind: "=?"
      },
      link: function($scope, element) {
        return latex($scope, element, latexService);
      },
      // template: "{{render(bind)}}",
      // template: "<div ng-bind-html='render(bind)'></div>"
      template: "<div ng-bind-html='render()' style='display:inline'></div>",
      restrict: "AE"
    };
  }]);

function latex($scope, element, latexService, katexConfig) 
{
  $scope.$watch("$scope.bind", onchange);

  function onchange()
  {

    console.log("onchange");

    $scope.bind = latexService.parse($scope.bind) || "";

    MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    MathJax.Hub.Config(MathJaxConfig);
  }

  $scope.render = function(bind) 
  {
    if (!$scope.bind) 
      return;

    return $scope.bind;

    console.log("render");

    MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    MathJax.Hub.Config(MathJaxConfig);

    return $scope.bind;
  };
}
var MathJaxConfig = {
  jax: ["input/TeX", "output/HTML-CSS"],
  tex2jax: {
    inlineMath: [['$','$'], ['\\(','\\)']],
    displayMath: [ ['$$','$$'], ['\[','\]'], ['\begin{align*}', '\end{align*}'] ]
    },
  TeX: {
    TagSide: "left",
    Macros: {
      webonly: '{}',
      texonly: ['#1', 1],
      set: ['\\left\\lbrace #1 \\right\\rbrace', 1],
      setc: ['\\left\\lbrace #1 \\mid #2\\right\\rbrace', 2],
      then: '\\;\\Longrightarrow\\;',
      qm: ["``#1''", 1],
      capital: ['\\MakeUppercase{#1}', 1],
      titlemath: ['\\texorpdfstring{$#1$}{}', 1],
      titlebmath: ['\\boldmath \\texorpdfstring{$#1$}{}', 1],
      skprod: ['\\left\\langle #1, #2 \\right\\rangle', 2],
      norm: ['\\|#1\\|', 1],
      floor: ['\\lfloor #1 \\rfloor', 1],
      ceiling: ['\\lceil #1 \\rceil', 1],
      subset: '{\\subseteq}',
      supset: '{\\supseteq}',
      id: '{id}',
      divergence: '{div}',
      support: '{supp}',
      graph: '{Graph}',
      sgn: '{sgn}',
      re: '{Re}',
      im: '{Im}',
      ii: '{\\mathrm{i}}',
      euler: '{\\mathrm{e}}',
      de: '{\\thinspace{\\rm{d}}}',
      De: '{\\mathrm D}',
      vk: ['\\mathbf{#1}', 1],
      metrik: '{\\mathrm{d}}',
      tangent: '{\\mathbb T}',
      boldpartial: '{\\pmb{\\partial}}',
      real: ['\\overline{#1}', 1],
      arsinh: '{arsinh}',
      arcosh: '{arcosh}',
      artanh: '{artanh}',
      Si: '{Si}',
      Ci: '{Ci}',
      lims: ['\\mathchoice{\\underset{#1}{\\overline{\\lim}}}{\\overline{\\lim}_{#1}\, }{\\overline{\\lim}_{#1}\\, }{\\overline{\\lim}_{#1}\\, }', 1],
      limi: ['\\mathchoice{\\underset{#1}{\\underline{\\lim}}}{\\underline{\\lim}_{#1}\\, }{\\underline{\\lim}_{#1}\\, }{\\underline{\\lim}_{#1}\\, }', 1],
      Tr: '{Tr}',
      Mat: '{Mat}',
      R: '{\\mathbb R}',
      N: '{\\mathbb N}',
      Z: '{\\mathbb Z}',
      Q: '{\\mathbb Q}',
      C: '{\\mathbb C}',
      BF: '{\\mathbb Q}',
      BK: '{\\mathbb F}',
      T: '{T}',
      SO: '{SO}',
      GL: '{GL}',
      SL: '{SL}'
    },
    equationNumbers: {
      autoNumber: "AMS"
    }
  }
};

})();