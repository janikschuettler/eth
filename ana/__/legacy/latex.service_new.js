/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {
"use strict";

angular
  .module("app")
  .service("latexService", LatexService);

LatexService.$inject = [];

function LatexService() 
{
  var self = this;

  var parser = new Parser();

  // efficiency
  var lastCalled = time();
  var callInterval = 1000;

  self.parse = parse;
  self.updateLatex = updateLatex;

  activate();

  ////////////////

  function activate() {}


  function parse(text)
  {
    if (!text)
      return "";

    var keywords = [
      {
        openTag: "\\textbf{",
        closeTag: "}",
        handler: function(text) {return "<b>" + text + "</b>"}
      }, {
        openTag: "\\emph{",
        closeTag: "}",
        handler: function(text) {return "<i>" + text + "</i>"}
      }, {
        openTag: "\\qm{",
        closeTag: "}",
        handler: function(text) {return "\"" + text + "\""}
      }, {
        openTag: "\\begin{itemize}",
        closeTag: "\\end{itemize}",
        handler: listKeywordHandler
      }, {
        openTag: "\\begin{enumerate}",
        closeTag: "\\end{enumerate}",
        handler: listKeywordHandler
      }, {
        openTag: "\\index[symb]{",
        closeTag: "}",
        handler: function() {return ""}
      }, /*{
        openTag: "\\nomenclature{",
        closeTag: "}",
        handler: function() {return ""}
      },*/ {
        openTag: "\\label{",
        closeTag: "}",
        handler: function() {return ""}
      }
    ];

    text = text.replace(/</g, "&lt;");
    text = text.replace(/>/g, "&gt;");
    text = text.replace(/\\enumnewline/g, "");

    /*if (text.substr(0, 7) == "\\label{") {
      bracketSplit = text.split("}");
      text = text.replace(bracketSplit[0] + "}", "");
    }*/

    var parsed = parser.parse(text, keywords);

    parsed = parsed.replace(/\n\s*\n/, "<br>");

    return parsed || text;
  }

  function listKeywordHandler(text, object)
  {
    var newText = "<ul>";

    if (text[0] == "[") 
    {
      var bracketSplit = text.split("]");
      var bracketContent = bracketSplit[0].substr(1);
      text = text.replace("[" + bracketContent + "]", "").trim();
    }

    // TODO remove [(i)] from beginning

    var items = text.split("\\item");
    items.forEach(function(item) {
      item = item.trim();
      if (item)
        newText += "<li>" + item + "</li>";
    });

    newText += "</ul>";

    return newText;
  }

  function updateLatex()
  {
    /*if (Math.abs(time() - lastCalled) < callInterval) 
    {
      console.log("stopping");
      lastCalled = time();
    }
    else {
      console.log(time() + " - rendering, " + Math.abs(time() - lastCalled));
      console.log(lastCalled);
      lastCalled = time();*/
      // return;
      // setTimeout(function() {

        console.log("here to render yayy");
        try {
          MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
          MathJax.Hub.Config(MathJaxConfig);
          return true;
        }
        catch (e) {
          return false;
        }

      // }, 100);
    // }
  }

  function time()
  {
    return new Date().getTime();
  }
}

function Parser()
{
  var self = this;

  self.keyword = {};

  //
  self.parse = parse;

  ////////////////

  function parse(string, keywords)
  {
    return looper(string, keywords || self.keywords);
  }

  function looper(string, keywords)
  {
    var newString = String(string);

    for (var char = 0; char < string.length; char++)
    {
      var result = checkForKeyword(string, char, keywords, "openTag");

      if (result)
      {
        var endTagSplit  = string.substr(char).split(result.closeTag); //explode($result["closeTag"], );
        var innerContent = endTagSplit[0];

        if (result.closeTag == "}")
        {
          var number = 1;
          while (innerContent.split("{").length > number)
          {
            innerContent = innerContent + "}" + endTagSplit[number++];
          }
        }

        var outerContent = result.openTag + innerContent + result.closeTag;

        var handlerArguments = {
          "keyword": result,
          "content": {
            // "outer" => substr($fileContent, char-strlen($result["openTag"]), strlen($result["openTag"])) . $innerContent . substr($fileContent, $char+strlen($innerContent), strlen($result["closeTag"])),
            "outer": outerContent,
            "inner": innerContent
          }
        };

        if (result.handler)
        {
          var newOuterContent = result.handler(innerContent, handlerArguments);
          newString = newString.replace(outerContent, newOuterContent);
        }
      }
    }
    return newString;
  }

  function checkForKeyword(string, position, needles, property)
  {
    for (var i = 0; i < needles.length; i++)
    {
      var query = needles[i];
      var needle = query[property];
      var slice = string.substr(position - needle.length, needle.length);

      // if (string.substr(position - needle.length, position).indexOf(needle) > -1)
      if (slice == needle)
        return query;
    }

    return false;

    /*foreach($needles as $query)
    {
      $needle = $query[$property];
      if(strpos(substr($string, $position - strlen($needle), strlen($needle)), $needle, $offset) !== false) 
        return $query;
    }
    return false;*/
  }
}

var MathJaxConfig = {
  jax: ["input/TeX", "output/HTML-CSS"],
  tex2jax: {
    inlineMath: [['$','$'], ['\\(','\\)']],
    displayMath: [ ['$$','$$'], ['\[','\]'], ['\begin{align*}', '\end{align*}'] ]
    },
  TeX: {
    TagSide: "left",
    Macros: {
      webonly: '{}',
      texonly: ['#1', 1],
      set: ['\\left\\lbrace #1 \\right\\rbrace', 1],
      setc: ['\\left\\lbrace #1 \\mid #2\\right\\rbrace', 2],
      then: '\\;\\Longrightarrow\\;',
      qm: ["``#1''", 1],
      capital: ['\\MakeUppercase{#1}', 1],
      titlemath: ['\\texorpdfstring{$#1$}{}', 1],
      titlebmath: ['\\boldmath \\texorpdfstring{$#1$}{}', 1],
      skprod: ['\\left\\langle #1, #2 \\right\\rangle', 2],
      norm: ['\\|#1\\|', 1],
      floor: ['\\lfloor #1 \\rfloor', 1],
      ceiling: ['\\lceil #1 \\rceil', 1],
      subset: '{\\subseteq}',
      supset: '{\\supseteq}',
      id: '{id}',
      divergence: '{div}',
      support: '{supp}',
      graph: '{Graph}',
      sgn: '{sgn}',
      re: '{Re}',
      im: '{Im}',
      ii: '{\\mathrm{i}}',
      euler: '{\\mathrm{e}}',
      de: '{\\thinspace{\\rm{d}}}',
      De: '{\\mathrm D}',
      vk: ['\\mathbf{#1}', 1],
      metrik: '{\\mathrm{d}}',
      tangent: '{\\mathbb T}',
      boldpartial: '{\\pmb{\\partial}}',
      real: ['\\overline{#1}', 1],
      arsinh: '{arsinh}',
      arcosh: '{arcosh}',
      artanh: '{artanh}',
      Si: '{Si}',
      Ci: '{Ci}',
      lims: ['\\mathchoice{\\underset{#1}{\\overline{\\lim}}}{\\overline{\\lim}_{#1}\, }{\\overline{\\lim}_{#1}\\, }{\\overline{\\lim}_{#1}\\, }', 1],
      limi: ['\\mathchoice{\\underset{#1}{\\underline{\\lim}}}{\\underline{\\lim}_{#1}\\, }{\\underline{\\lim}_{#1}\\, }{\\underline{\\lim}_{#1}\\, }', 1],
      Tr: '{Tr}',
      Mat: '{Mat}',
      R: '{\\mathbb R}',
      N: '{\\mathbb N}',
      Z: '{\\mathbb Z}',
      Q: '{\\mathbb Q}',
      C: '{\\mathbb C}',
      BF: '{\\mathbb Q}',
      BK: '{\\mathbb F}',
      T: '{T}',
      SO: '{SO}',
      GL: '{GL}',
      SL: '{SL}'
    },
    equationNumbers: {
      autoNumber: "AMS"
    }
  }
};

})();
