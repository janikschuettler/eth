/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {
"use strict";

angular
  .module("app")
  .controller("EditViewController", EditViewController);

EditViewController.$inject = [
  "$scope", 
  "$sce", 
  "dataService", 
  "idHandler", 
  "$timeout",
  "$routeParams", 
  "$location",
  "latexConfig"
];

function EditViewController(
    $scope, 
    $sce, 
    dataService, 
    idHandler, 
    $timeout, 
    $routeParams, 
    $location, 
    latexConfig
  ) 
{
  var self = $scope;

  // data
  self.form = {};
  self.theorems = [];

  // progress
  self.requestStatus = {};

  self.lectureBaseUrl = "http://www.video.ethz.ch/etc/designs/mmp/paella/video.html";
  self.lectureUrl = "";
  self.latexLabelIsUnique = true;

  self.userFeedback = {};


  // func
  self.save = save;
  self.resize = resize;
  self.showNext = showNext;
  self.showPrevious = showPrevious;
  self.loadTheorem = loadTheorem;
  self.renderLectureUrl = renderLectureUrl;
  self.latexLabelInputWasChanged = latexLabelInputWasChanged;

  self.$on("databaseFinishedLoading", onload);

  activate();

  ////////
  function activate() {}

  function onload()
  {
    self.data = {
      theorems: dataService.theorems.data,
      definitions: dataService.definitions.data,
      lectures: dataService.lectures.data,
      chapters: dataService.chapters.data
    };

    latexConfig.refLinkHandler = function(label, theorem) {
      return "<a href='?theoremId=" + theorem.id + "'>" + theorem.lecture_note_id + "</a>";
    };

    if ($routeParams.theoremId)
      loadTheorem($routeParams.theoremId);
  }

  function formIsValid()
  {
    self.userFeedback = {};

    var formIsValid = 
      self.latexLabelIsUnique &&
      // self.form.theorems.lecture_note_id &&
      (self.form.theorems.summary || self.form.theorems.name) &&
      self.form.theorems.type_id &&
      self.form.theorems.importance &&
      self.form.theorems.lecture_id
      // && self.form.statements.length > 0 &&
      // && (self.form.premises.length == 0 || self.form.premises_to_statements.length > 0);

    var inputIsMissingErrorObject = {
      message: "Dieses Feld darf nicht leer sein.",
      classLabel: "has-danger"
    };

    if (!self.latexLabelIsUnique) {}
    // if (!self.form.theorems.lecture_note_id) {
    //   self.userFeedback.lecture_note_id = inputIsMissingErrorObject;
    // }
    if (!(self.form.theorems.summary || self.form.theorems.name)) {
      var atLeastOneInputErrorObject = {
        message: "Mindestens Name oder Zusammenfassung muss ausgefüllt sein.",
        classLabel: "has-danger"
      };
      self.userFeedback.name = atLeastOneInputErrorObject;
      self.userFeedback.summary = atLeastOneInputErrorObject;
    }
    if (!self.form.theorems.type_id) {
      self.userFeedback.type_id = inputIsMissingErrorObject;
    }
    if (!self.form.theorems.importance) {
      self.userFeedback.importance = inputIsMissingErrorObject;
    }
    if (!self.form.theorems.lecture_id) {
      self.userFeedback.lecture_id = inputIsMissingErrorObject;
    }
    // if (self.form.statements.length == 0) {
    //   self.userFeedback.statements = inputIsMissingErrorObject;
    // }
    // if (!(self.form.premises.length == 0 || self.form.premises_to_statements.length > 0)) {
    //   self.userFeedback.premises_to_statements = {
    //     message: "Die Voraussetzungen müssen mindestens eine Verbindung zu Aussagen haben.",
    //     classLabel: "has-danger"
    //   };
    // }

    return formIsValid;
  }

  function save() 
  {
    // write directives to check if form is valid and to show or disable create button accordingly
    if (!formIsValid())
    {
      console.log("form not valid");
      return;
    }

    console.log("form valid");

    self.isInProgress = true;

    var theoremObject = {};

    theoremObject.theorem = self.form.theorems;
    theoremObject.proofs = [];
    theoremObject.proofs[0] = self.form.proofs;
    theoremObject.linkedTheorems = self.form.linkedTheorems;
    theoremObject.linkedDefinitions = self.form.linkedDefinitions;

    var promise = dataService.setTheoremObject(theoremObject);

    promise
      .then(function(result) {

        self.requestStatus.success = true;
        self.requestStatus.userHeading = "Eingabe war erfolgreich";
        self.requestStatus.userMessage = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus aliquid, enim delectus ex fugit nihil voluptatum harum, accusantium rem voluptatem quis quasi fuga numquam recusandae, cum a facilis magni praesentium.";

      }, function(result) {

        var failedRequestObject = result.responses[result.responses.length-1];
        self.requestStatus.success = false;
        self.requestStatus.userHeading = "Eingabe ist fehlgeschlagen";
        self.requestStatus.userMessage = failedRequestObject.response.error + " [" + failedRequestObject.request.config.data.sql + "]";
        console.error("Edit failed");

        var errorMessage = "Error Message: \n \n";
            errorMessage += JSON.stringify(failedRequestObject.response, null, 4);
            errorMessage += "\n \n For request: \n \n:";
            errorMessage += JSON.stringify(failedRequestObject.request.config.data);

        // sendMail.send("Error with 'New | Ana'", errorMessage);
      })
      .finally(function() {
        self.requestStatus.done = true;
        self.isInProgress = false;
        $timeout(function() {
          self.requestStatus.done = false;
        }, 5 * 1000);
      });
  }

  function latexLabelInputWasChanged()
  {
    if (self.newTheoremService.data.latexLabels.indexOf(self.form.theorems.latex_label) > -1){
      self.latexLabelIsUnique = false;
    }
    else {
      self.latexLabelIsUnique = true;
    }
  }

  function loadTheorem(theoremId)
  {
    var theoremObject = dataService.getTheoremObject(theoremId);

    self.form.theorems = theoremObject.theorem;
    self.form.proofs = theoremObject.proofs[0];
    self.form.linkedTheorems = theoremObject.linkedTheorems;
    self.form.linkedDefinitions = theoremObject.linkedDefinitions;

    self.requestStatus.done = false;

    renderLectureUrl();

    $location.search("theoremId", theoremId);
  }

  function showPrevious()
  {
    loadTheorem(parseInt(self.form.theorems.id) - 1);
  }

  function showNext()
  {
    loadTheorem(parseInt(self.form.theorems.id) + 1);
  }

  function resize(element) 
  {
    element.currentTarget.style.height = "5px";
    element.currentTarget.style.height = (element.currentTarget.scrollHeight)+"px";
  }

  function renderLectureUrl()
  {
    var newId   = parseInt(self.form.theorems.lecture_id),
        lecture = dataService.lectures[newId];

    if (!lecture) {
      return;
    }

    self.lectureUrl = $sce.trustAsResourceUrl(self.lectureBaseUrl + "?id=" + lecture.video_id + "&time=" + self.form.theorems.video_position);
  }
}

})();

