/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

(function() {
  
"use strict";

angular
  .module("app")
  .service("newTheoremService", newTheoremService);

newTheoremService.$inject = ["$rootScope", "$q", "$http", "idHandler", "dataService"];

function newTheoremService($rootScope, $q, $http, idHandler, dataService)
{
  var self = this;

  var databaseTableIdCounter = {};
  var tempDatabaseTableIdCounter = {};

  var defaultHttpConfig = {
    headers: {
      "Content-Type": undefined
    }
  };

  // config
  self.databaseTablePrefix = "analysis_";

  // data
  self.data = {
    patterns: [],
    theorems: [],
    definitions: [],
    lectures: [],
    latexLabels: [],
    lectureNoteIds: []
  };


  // func
  self.createNewTheorem = createNewTheorem;

  activate();

  ////////

  function activate()
  {

    fetchIdCounter()
      .then(function(result) 
      {
        for (var table in result.data) {
           databaseTableIdCounter[table] = parseInt(result.data[table][0].counter) + 1;
        }
        console.log(databaseTableIdCounter);
      })
      .catch(function(result) {
        console.log("fetchIdCounter was not successful");
        console.log(result);
      });
  }

  $rootScope.$on("databaseFinishedLoading", function() 
  {
    self.data = {
      patterns: dataService.patterns.data,
      lectures: dataService.lectures.data,
      theorems: dataService.theorems.data,
      definitions: dataService.definitions.data,
      lectureNoteIds: dataService.lectureNoteIds,
      latexLabels: dataService.latexLabels
    }
  });

  function createNewTheorem(formData, chainCallback)
  {
    if (self.isInProgress)
      return;

    console.log(formData);

    self.isInProgress = true;
    tempDatabaseTableIdCounter = angular.copy(databaseTableIdCounter);

    var pipeline = generateSqlPipeline(formData);

    console.log(pipeline);

    return $q(function(resolve, reject) 
    {
      var progress = [];

      var resolveLoop = function()
      {
        var success = true;

        progress.forEach(function(stepResult) { if (!stepResult.success) success = false });

        var stepResults = {
          success: success,
          responses: progress
        };

        self.isInProgress = false;

        console.log(stepResults);

        if (success)
        {
          databaseTableIdCounter = tempDatabaseTableIdCounter;
          tempDatabaseTableIdCounter = {};

          self.data.theorems.push(formData.theorems);
          self.data.latexLabels.push(formData.theorems.latex_label);
          self.data.lectureNoteIds.push(formData.theorems.lecture_note_id);
          // update patterns, but only new ones
          // self.data.patterns = result.data;

          resolve(stepResults);
        }
        else 
        {
          reject(stepResults);
        }
      }

      var resolveHandler = function(success, response)
      {
        console.log(response);
        var stepResult = {
          success: success,
          response: response.data,
          result: response.data.result,
          request: response
        };
        progress.push(stepResult);
      };

      var sqlPiplineExecuter = function(argument) 
      {
        var sqlStatement = pipeline.shift();

        if (sqlStatement) 
        {
          console.log(sqlStatement);
          fetchData(sqlStatement)
            .then(function(response) {
              resolveHandler(true, response);
              sqlPiplineExecuter(argument);
            }, function(response) {
              resolveHandler(false, response);
              resolveLoop();
            });
        }
        else
        {
          resolveLoop();
        }
      }
      sqlPiplineExecuter();
    });
  }

  function fetchData(fromSql)
  {
    var data = {
      sql: fromSql
    };
    return $q(function(resolve, reject) {
      $http.post("http://js.vsos.ethz.ch/projects/eth/ana/__/php/select.php", data, defaultHttpConfig)
        .then(function(result) {
          if (result.data.__check)
            if (result.data.error || result.data.errorCode || result.data.errorList.length > 0)
              reject(result);
            else 
              resolve(result);
          else
            resolve(result);
        }, reject);
    });
  }

  function fetchIdCounter()
  {

    return $http.get("http://js.vsos.ethz.ch/projects/eth/ana/__/php/selectCount.php");
  }

  function createInsertSql(table, data)
  { 
    var data = Array.isArray(data) ? data : Array(data);

    if (data.length < 1)
      return "";

    var tableName = self.databaseTablePrefix + table;
    var sql = "INSERT INTO `" + tableName + "` ";

    var columns = "";
    var values = "";

    var keys = Object.keys(data[0]);

    keys.forEach(function(key) {
      if (key != "$$hashKey") 
        columns += "`" + key + "`, ";
    });
    columns = columns.substr(0, columns.length - 2);

    data.forEach(function(dataElement) 
    {
      if (Object.keys(dataElement).length != keys.length)
      {
        console.error("createInsertSql: Column and value length differ (" + table + ")");
        console.log(data);
        return "";
      }

      if (idHandler.idIsTemporary(dataElement.id)) 
      {
        values += "(";

        for (var key in dataElement)
        {
          if (key != "$$hashKey") 
          {
            var value = dataElement[key];
            value = (idHandler.idIsTemporary(dataElement[key])) ? replaceTempId(tableName, dataElement[key]) : value;
            // values += typeof value == "string" ? "'" + value + "', " : value + ", ";
            values += "'" + mysql_real_escape_string(value) + "', " ;
          }
        }

        values  = values.substr(0, values.length - 2);
        values += "), ";
      }
    });

    values = values.substr(0, values.length - 2);
    sql   += "(" + columns + ") VALUES " + values + ";";

    if (!values)
      return "";

    return sql;
  }

  function replaceTempId(tableName, tempId)
  {
    if (!idHandler.idIsTemporary(tempId))
      return tempId;

    var replaceTempId = idHandler.getReplacedId(tempId);

    if (replaceTempId) 
    {
      return replaceTempId;
    }
    else
    {
      return idHandler.replaceTempIdBy(tempId, tempDatabaseTableIdCounter[tableName]++);
    }
  }

  function generateSqlPipeline(formData)
  {
    var sql = {};

    sql.theorems   = createInsertSql("theorems", formData.theorems);
    sql.premises   = createInsertSql("patterns", formData.premises);
    sql.statements = createInsertSql("patterns", formData.statements);
    sql.premises_to_statements = createInsertSql("premises_to_statements", formData.premises_to_statements);
    sql.theorems_to_premises_to_statements = createInsertSql("theorems_to_premises_to_statements", createLinkTableDataObject(formData.premises_to_statements, "premises_to_statements_id", {
        key: "theorem_id",
        value: formData.theorems.id
      }));
    sql.proofs = formData.proofs.tex ? createInsertSql("proofs", formData.proofs) : "";
    sql.proofs_to_definitions = formData.proofs.tex ? createInsertSql("proofs_to_definitions", createLinkTableDataObject(formData.proofs_to_definitions, "definition_id", {
        key: "proofs_id",
        value: formData.proofs.id
      })) : "";
    sql.proofs_to_theorems = formData.proofs.tex ? createInsertSql("proofs_to_theorems", createLinkTableDataObject(formData.proofs_to_theorems, "theorem_id", {
        key: "proofs_id",
        value: formData.proofs.id
      })) : "";
    sql.theorems_to_proofs = formData.proofs.tex ? createInsertSql("theorems_to_proofs", formData.theorems_to_proofs) : "";
    sql.notes = createInsertSql("notes", formData.notes);
    sql.theorems_to_notes = createInsertSql("theorems_to_notes", createLinkTableDataObject(formData.notes, "notes_id", {
        key: "theorem_id",
        value: formData.theorems.id
      }));

    return [
        sql.theorems,
        sql.premises, sql.statements,
          sql.premises_to_statements,
            sql.theorems_to_premises_to_statements,
        sql.proofs,
          sql.proofs_to_definitions,
          sql.proofs_to_theorems,
          sql.theorems_to_proofs,
        sql.notes,
          sql.theorems_to_notes
      ].filter(function(element) { return element != "" });
  }

  function createLinkTableDataObject(data, keyName, constantObject)
  {
    var linkedData = [];
    data.forEach(function(dataElement) {
      var linkObject = {};
      linkObject.id  = idHandler.generateTempId();
      linkObject[keyName] = dataElement.id
      linkObject[constantObject.key] = constantObject.value;
      linkedData.push(linkObject);
    });
    return linkedData;
  }

  function mysql_real_escape_string (string) 
  {
    return String(string).replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
      switch (char) {
        case "\0":
          return "\\0";
        case "\x08":
          return "\\b";
        case "\x09":
          return "\\t";
        case "\x1a":
          return "\\z";
        case "\n":
          return "\\n";
        case "\r":
          return "\\r";
        case "\"":
        case "'":
        case "\\":
        case "%":
          return "\\"+char; // prepends a backslash to backslash, percent,
                            // and double/single quotes
      }
    });
  }
}




angular.module("app")
  .service("idHandler", idHandler);

function idHandler()
{
  var self = this;

  // var  
  var tempIdentifier = "__temp";
  var emptyTempIdValue = false;

  self.generatedTempIds = {};

  // func
  self.generateTempId  = generateTempId;
  self.idIsTemporary   = idIsTemporary;
  self.getReplacedId   = getReplacedId;
  self.replaceTempIdBy = replaceTempIdBy;

  ///////

  function generateTempId()
  {
    do {
      // var randomTempId = tempIdentifier + Math.round(new Date().getTime() * Math.random());
      var randomTempId = tempIdentifier + new Date().getTime() + Math.random();
    } while (randomTempId in self.generatedTempIds);

    self.generatedTempIds[randomTempId] = emptyTempIdValue;

    return randomTempId;
  }

  function idIsTemporary(id)
  {
    return (typeof id == "string" && id.substr(0, tempIdentifier.length) == tempIdentifier);
  }

  function replaceTempIdBy(tempId, newId)
  {
    self.generatedTempIds[tempId] = newId;
    return newId;
  }

  function getReplacedId(tempId)
  {
    return (checkIfTempIdHasBeenReplaced) ? self.generatedTempIds[tempId] : false;
  }

  function checkIfTempIdHasBeenReplaced(tempId)
  {
    return self.generatedTempIds[tempId] === emptyTempIdValue;
  }

}
})();