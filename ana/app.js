/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

/*
 * TODO
 * 7.39, 6.31, 2.51, 2.25, 11.38, 2.22, 8.5, 2.20, 2.25, 4.4, 2.22, 3.15, 2.27, 2.30
*/


Array.prototype.last = function() {
  return this[this.length - 1];
};

angular.module("app", ["ngRoute", "ngTouch", "ngSanitize", "cfp.hotkeys", "latex"]);


// @codekit-append "service/database.service.js";
// @codekit-append "service/data.service.js";
// @codekit-append "service/settings.service.js";
// @codekit-append "module/latex.module.js"
// @codekit-append "keyboardManager.factory.js"
// @codekit-append "navigationView/navigationView.controller.js";
// @codekit-append "studyView/studyView.controller.js";
// @codekit-append "listView/listView.controller.js";
// @codekit-append "graphView/graphView.controller.js";
// @codekit-append "detailView/detailView.controller.js";
// @codekit-append "settingsView/settingsView.controller.js";
// @codekit-append "config/app.config.js";




