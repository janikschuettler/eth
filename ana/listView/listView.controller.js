/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {

"use strict";

angular
  .module("app")
  .controller("ListViewController", ListViewController);

ListViewController.$inject = ["$scope", "dataService", "settings", "katexConfig"];

function ListViewController($scope, dataService, settings, katexConfig) {

  var self = $scope;

  self.elements = [];

  self.strings = {
    theoremColors: {
      "6": "success",    // definition
      "2": "primary", // proposition
      "1": "danger",  // theorem
      "1": "danger",  // satz
      "3": "warning", // lemma
      "4": "info", // korollar
      "5": "", // axiom
      bemerkung: "",  // bemerkung
      notiz: "" // notiz
    }
  };

  $scope.autoRenderTex = 'Exponentiation:<br>\\(x^2\\) and \\(x^3\\)';

  self.userSettings = {};

  var defaultSettings = {
    showType: {
      "1": true,
      "2": true,
      "3": true,
      "4": true,
      "5": false,
      "6": true
    },
    showChapters: false,
    exists: "yes"
  };


  self.getChapterTitleByNumber = getChapterTitleByNumber;
  self.getElementType = getElementType;

  activate();

  ////////
  Object.defineProperties(self, {});

  self.$on("databaseFinishedLoading", onload);
  self.$watch("userSettings", userSettingsChanged, true);

  ////////
  function activate()
  {
    self.userSettings = settings.list ? settings.list : defaultSettings;

    if (dataService.isLoaded) {
      onload();
    }

    var delimiters = [
        {left: "$$", right: "$$", display: true},
        {left: "\\[", right: "\\]", display: true},
        {left: "\\(", right: "\\)", display: false},
        {left: "$", right: "$", display: false},
        {left: "\\begin{align}", right: "\\end{align}", display: true},
        {left: "\\begin{align*}", right: "\\end{align*}", display: true}
      ];

    katexConfig.defaultOptions.delimiters = delimiters;
    katexConfig.defaultOptions.throwOnError = false;
    katexConfig.defaultOptions.displayMode = true;
    katexConfig.defaultOptions.errorColor = '#d85';
  }

  function onload() 
  {
    self.elements = dataService.getTheoremsAndDefinitionsByChapters();
  }

  function userSettingsChanged(newValue)
  {
    settings.list = {};
    for (var key in newValue)
    {
      settings.list[key] = newValue[key];
    }
  }

  function getChapterTitleByNumber(chapterNumber)
  {
    for (var i = 0; i < dataService.chapters.length; i++)
      if (dataService.chapters.data[i].number == chapterNumber)
        return dataService.chapters.data[i].title;

    return "";
  }

  function getElementType(elementTypeId)
  {
    var type = dataService.types_of_theorems[elementTypeId].type;
    return type;
  }

}

})();

