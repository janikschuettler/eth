/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

(function() {

"use strict";

angular.module("app")
  .config(AppConfig);

AppConfig.$inject = ["$routeProvider", "$locationProvider", "$httpProvider", "$sceDelegateProvider", "latexConfigProvider"];

function AppConfig($routeProvider, $locationProvider, $httpProvider, $sceDelegateProvider, latexConfigProvider) 
{
  // set app-wide atex configuration as defined in katex.config.js
  latexConfigProvider.configure(window.katexConfig);

  $routeProvider
    .when("/", {
        redirectTo: "/study"
    })
    .when("/study", { 
        templateUrl: "studyView/study.template.html",
        controller: "StudyViewController"
    })
    .when("/study/chapter/:studyChapter", { 
        templateUrl: "studyView/study.template.html",
        controller: "StudyViewController"
    })
    .when("/list", { 
        templateUrl: "listView/list.template.html",
        controller: "ListViewController"
    })

    .when("/detail", {
        redirectTo: "/list"
    })
    .when("/detail/:id", { 
        templateUrl: "detailView/detail.template.html",
        controller: "DetailViewController"
    })
    .when("/graph", { 
        templateUrl: "graphView/graph.template.html", 
        controller: "GraphViewController"
    })
    .when("/graph/chapter/:studyChapter", { 
        templateUrl: "graphView/graph.template.html", 
        controller: "GraphViewController"
    })
    .when("/settings", { 
        templateUrl: "settingsView/settings.template.html", 
        controller: "SettingsViewController"
    })
    .otherwise({ 
        redirectTo: "/"
    });  

  $locationProvider.html5Mode(true);

  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    "self",
    // Allow loading from our assets domain.  Notice the difference between * and **.
    "http://www.video.ethz.ch/etc/designs/mmp/paella/video.html/**",
    "http://js.vsos.ethz.ch/**"
  ]); 
}
})();
