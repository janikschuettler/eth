/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */

var katexConfig = {
  delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false},
    {left: "\\[", right: "\\]", display: true},
    {left: "\\begin{align}", right: "\\end{align}", display: true},
    {left: "\\begin{align*}", right: "\\end{align*}", display: true},
    {left: "\\(", right: "\\)", display: false}
  ],
  mathjax: {
    showProcessingMessages: false,
    jax: ["input/TeX", "output/HTML-CSS"],
    tex2jax: {
        inlineMath: [['$','$'], ['~$','$'], ['\\(','\\)']]
      },
    TeX: {
      TagSide: "left",
      equationNumbers: {
        autoNumber: "AMS"
      }
    }
  },
  macros: {
    "\\webonly":"{}",
    "\\texonly":"#1",
    "\\set":"\\left\\lbrace #1 \\right\\rbrace",
    "\\setc":"\\left\\lbrace #1 \\mid #2\\right\\rbrace",
    "\\then":"\\;\\Longrightarrow\\;",
    "\\qm":"``#1''",
    "\\capital":"\\MakeUppercase{#1}",
    "\\titlemath":"\\texorpdfstring{$#1$}{}",
    "\\titlebmath":"\\boldmath \\texorpdfstring{$#1$}{}",
    "\\skprod":"\\left\\langle #1, #2 \\right\\rangle",
    "\\norm":"\\|#1\\|",
    "\\floor":"\\lfloor #1 \\rfloor",
    "\\ceiling":"\\lceil #1 \\rceil",
    "\\one":"\\mathbb 1",
    "\\subset":"{\\subseteq}",
    "\\supset":"{\\supseteq}",
    "\\id":"{id}",
    "\\div":"{div}",
    "\\laplace":"\\Delta",
    "\\rot":"rot",
    "\\vol":"vol",
    "\\support":"{supp}",
    "\\graph":"{Graph}",
    "\\sgn":"{sgn}",
    "\\re":"{Re}",
    "\\im":"{Im}",
    "\\ii":"{\\mathrm{i}}",
    "\\euler":"{\\mathrm{e}}",
    "\\de":"{\\, {\\rm{d}}}",
    "\\De":"{\\mathrm D}",
    "\\vk":"\\mathbf{#1}",
    "\\metrik":"{\\mathrm{d}}",
    "\\tangent":"{\\mathbb T}",
    "\\boldpartial":"{\\pmb{\\partial}}",
    "\\real":"\\overline{#1}",
    "\\arsinh":"{arsinh}",
    "\\arcosh":"{arcosh}",
    "\\artanh":"{artanh}",
    "\\Si":"{Si}",
    "\\Ci":"{Ci}",
    "\\lims":"\\mathchoice{\\underset{#1}{\\overline{\\lim}}}{\\overline{\\lim}_{#1}, }{\\overline{\\lim}_{#1}\\, }{\\overline{\\lim}_{#1}\\, }",
    "\\limi":"\\mathchoice{\\underset{#1}{\\underline{\\lim}}}{\\underline{\\lim}_{#1}\\, }{\\underline{\\lim}_{#1}\\, }{\\underline{\\lim}_{#1}\\, }",
    "\\Tr":"{Tr}",
    "\\Mat":"{Mat}",
    "\\R":"{\\mathbb R}",
    "\\N":"{\\mathbb N}",
    "\\Z":"{\\mathbb Z}",
    "\\Q":"{\\mathbb Q}",
    "\\C":"{\\mathbb C}",
    "\\BF":"{\\mathbb Q}",
    "\\BK":"{\\mathbb F}",
    "\\T":"{T}",
    "\\SO":"{SO}",
    "\\GL":"{GL}",
    "\\SL":"{SL}",
    "\\textup":"{\\mathrm{#1}}"
  }
};