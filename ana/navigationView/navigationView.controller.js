/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {
"use strict";

angular
  .module("app")
  .controller("navigationViewController", NavigationViewController);

NavigationViewController.$inject = ["$scope", "$route", "$routeParams", "latexConfig"];

function NavigationViewController($scope, $route, $routeParams, latexConfig) 
{
  var self = $scope;
  self.title = "NavigationViewController";

  self.hideNavigation = true;
  self.subdirectory = window.location.pathname;

  self.reload = reload;

  activate();

  ////////////////

  function activate() 
  {
    latexConfig.refLinkHandler = function(label, theorem) {
      return "<a href='detail/" + theorem.id + "'>" + theorem.lecture_note_id + "</a>";
    };
  }

  $scope.$on('$routeChangeSuccess', function() {
    self.hideNavigation = $routeParams.hideNavigation === "true";
  });

  function reload() {
    $route.reload();
  }
}
})();