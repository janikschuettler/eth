/*!
 * angular-katex v0.9.0
 * https://github.com/tfoxy/angular-katex
 *
 * Copyright 2015 Tomás Fox
 * Released under the MIT license
 */

(function() {
  'use strict';

  angular.module('katex', [])
      .constant('katex', katex)
      .constant('renderMathInElement', getRenderMathInElement())
      .provider('katexConfig', katexConfigProvider)
      .directive('katex', katexDirective);


  function getRenderMathInElement() {
    return typeof renderMathInElement !== 'undefined' ?
        renderMathInElement :
        undefined;
  }


  katexConfigProvider.$inject = ['katex', 'renderMathInElement'];

  function katexConfigProvider(katex, renderMathInElement) {
    var service = {
      $get: function() {return service;},
      defaultOptions: {},
      errorElement: '<span class="katex-error"></span>',
      configure: function(options, preprocessHandler) {
        var mathJaxOptions = options.mathjax || {};
            mathJaxOptions.TeX = mathJaxOptions.TeX || {};
            mathJaxOptions.TeX.Macros = convertKatexMacrosToMathJax(options.macros);

        service.defaultOptions = options;
        service.defaultMathJaxOptions = mathJaxOptions;
        service.preprocessHandler = preprocessHandler || service.preprocessHandler;

        MathJax.Hub.Config(mathJaxOptions);
        MathJax.Hub.Configured();
      },
      preprocessHandler: function(string) {return string},
      errorHandler: function(err, expr, element) {
        var span = angular.element(service.errorElement);
        span.text(err);
        element.children().remove();
        element.append(span);
      },
      render: function render(element, expr, scope, attrs) {
        try {
          var options = getOptions(scope, attrs);
          katex.render(expr || '', element[0], options);
        } catch (err) {
          getErrorHandler(scope, attrs)(err, expr, element);
        }
      },
      autoRender: function autoRender(element, scope, attrs) {
        element[0].innerHTML = service.preprocessHandler(element[0].innerHTML);
        try {
          var options = getOptions(scope, attrs);
              options.errorCallback = function(error) {
                throw(error);
              };
          renderMathInElement(element[0], options);
        } catch (error) {
          // getErrorHandler(scope, attrs)(error, null, element);
          MathJax.Hub.Queue(["Typeset", MathJax.Hub, element[0]]);
        }
      }
    };

    return service;

    function getOptions(scope, attrs) {
      var options = angular.extend({}, service.defaultOptions);
      if ('options' in attrs) {
        angular.extend(options, scope.$eval(attrs.options));
      }
      if ('katexOptions' in attrs) {
        angular.extend(options, scope.$eval(attrs.katexOptions));
      }
      if ('displayMode' in attrs) {
        angular.extend(options, {displayMode: true});
      }
      return options;
    }

    function getMathJaxOptions(scope, attrs) {
      var katexOptions = getOptions(scope, attrs);
      var mathJaxOptions = katexOptions.mathjax || {};
          mathJaxOptions.TeX = mathJaxOptions.TeX || {};
          mathJaxOptions.TeX.Macros = convertKatexMacrosToMathJax(katexOptions.macros);
      return mathJaxOptions;
    }

    function getErrorHandler(scope, attrs) {
      if ('onError' in attrs || 'katexOnError' in attrs) {
        return function katexOnErrorFn(err, expr, element) {
          scope.$eval(attrs.onError || attrs.katexOnError, {
            $err: err,
            $expr: expr,
            $setText: function(text) {
              element.text(text);
            }
          });
        };
      } else {
        return service.errorHandler;
      }
    }
  }


  katexDirective.$inject = ['katexConfig', '$rootScope'];

  function katexDirective(katexConfig, $rootScope) {
    return {
      restrict: 'AE',
      compile: compile
    };

    function compile(element, attrs) {
      if ('bind' in attrs) {
        return link;
      } else {
        var expr;

        if ('expr' in attrs || attrs.katex) {
          expr = attrs.expr || attrs.katex;
          if (hasHtmlModeOn($rootScope, attrs)) {
            expr = angular.element('<div>' + expr + '</div>').html();
          }
          if ('autoRender' in attrs || 'katexAutoRender' in attrs) {
            element.text(expr);
            katexConfig.autoRender(element, $rootScope, attrs);
            return;
          }
        } else if ('autoRender' in attrs || 'katexAutoRender' in attrs) {
          katexConfig.autoRender(element, $rootScope, attrs);
          return;
        } else {
          expr = hasHtmlModeOn($rootScope, attrs) ?
              element.html() :
              element.text();
        }

        katexConfig.render(element, expr, $rootScope, attrs);
      }
    }

    function link(scope, element, attrs) {
      if ('autoRender' in attrs || 'katexAutoRender' in attrs) {
        scope.$watch(attrs.bind, function(expr) {
          // if (hasHtmlModeOff(scope, attrs)) {
            element.text(expr);
          // } else {
            // element.html(expr);
          // }
          katexConfig.autoRender(element, scope, attrs);
        });
      } else {
        scope.$watch(attrs.bind, function(expr) {
          katexConfig.render(element, expr, scope, attrs);
        });
      }
    }
  }


  function hasHtmlModeOn(scope, attrs) {
    return 'htmlMode' in attrs && (!attrs.htmlMode || scope.$eval(attrs.htmlMode));
  }

  function hasHtmlModeOff(scope, attrs) {
    return 'htmlMode' in attrs && !scope.$eval(attrs.htmlMode);
  }


  function convertKatexMacrosToMathJax(macros)
  {
    var newMacros = {};

    for (var macroName in macros)
    {
      var value = macros[macroName];
      var argumentCount = Math.max.apply(Math, value.split("#").map(function(e){return parseInt(e[0])||0}));
      newMacros[macroName.substr(1)] = argumentCount > 0 ? [value, argumentCount] : value; 
    }

    return newMacros;
  }
})();