/*
 * Copyright (c) 2017 Janik S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */


(function() {
"use strict";

angular
  .module("app")
  .controller("thermoViewController", ThermoViewController);

ThermoViewController.$inject = ["$scope", "dataService"];

function ThermoViewController($scope, dataService) 
{
  var self = $scope;
  self.title = "ThermoViewController";

  self.test = "x^2";
  self.hasAllSources = true;
  self.tableVariable = "A";
  self.tableVariables = ["A", "G", "H", "S", "U"];

  self.renderMath = window.renderMathWithDelay;

  activate();

  ////////////////

  function activate() 
  {
    console.log(dataService);
    dataService.loadData("__/data/thermo.json")
      .then(function(result) {
        self.data = proccessData(result);
        self.tableData = processTableData(self.data);
      });
  }

  function proccessData(data)
  {
    // check if everything includes a source
    var hasAllSources = true;

    var checkForMissingSource = function(toCheck, element, variable) 
    {
      hasAllSources = hasAllSources && !!toCheck.src;
      if (!toCheck.src) 
      {
        console.log("Missing source for the following entry of variable " + data.variables[variable].name);
        console.log(element);
        return false;
      }
      return true;
    };

    for (var variable in data.variables)
    {
      if (data.variables[variable].differentials) 
      {
        data.variables[variable].differentials.forEach(function(differential) 
        {
          if (!differential.tex) 
          {
            var baseTex = "\\left( \\frac{\\partial " + data.variables[variable].tex + "}{\\partial " + data.variables[differential.by].tex + "} \\right)";

            // sources
            if (differential.multipleConst || Array.isArray(differential.is))
            {
              if (differential.multipleConst)
                differential.multipleConst.forEach(checkForMissingSource, differential, variable);
   
              if (Array.isArray(differential.is))
                differential.is.forEach(checkForMissingSource, differential, variable);
            }
            else
              checkForMissingSource(differential, differential, variable);

            // const
            var createConstTexString = function(constants) {
              return constants ? "_{" + constants.map(function(constVariable) {
                return data.variables[constVariable].tex || constVariable;
              }).join(", ") + "}" : "";
            };

            if (differential.multipleConst)
            {
              var baseTexCopy = String(baseTex);
                  baseTex = "";
              differential.multipleConst.forEach(function(constVariables) {
                baseTex += baseTexCopy + createConstTexString(constVariables.const) + " = ";
              });
              baseTex = baseTex.substr(0, baseTex.length-3);
            }
            else {
              baseTex += createConstTexString(differential.const);
            }

            // is
            var is = Array.isArray(differential.is) ? differential.is : [differential.is],
                isTex = "";
            is.forEach(function(element) 
            {
              var premiseText = element.premises ? element.premises.map(function(premiseName) {
                var premise = data.premises[premiseName];
                return premise.short || premise.name;
              }).join(", ") : "";
              isTex += " \\stackrel{" + premiseText + "}{=} " + (element.is || element);
            });

            isTex = isTex.substr(16); // TODO this is not a good solution, it will fail if the first is has a premise

            differential.baseTex = baseTex;
            differential.isTex   = isTex;
            differential.tex     = baseTex + " = " + isTex;
          }
        });
      }

      data.variables[variable].definitions && data.variables[variable].definitions.forEach(function(definition) {
        checkForMissingSource(definition, definition, variable);
      });
    }

    self.hasAllSources = hasAllSources;

    console.log(hasAllSources);

    return data;
  }

  function processTableData(data)
  {
    var deriveByVariables = ["T", "V", "p", "n", "xi"];
    var deriveByVariableCombinations = [["T", "p"], ["T", "n"], ["T", "V"], ["V", "n"], ["V", "p"], ["p", "n"]];
    var tableData = {};

    self.tableVariables.forEach(function(baseVariable) 
    {
      var baseObject = {};
      deriveByVariables.forEach(function(diffVariable) 
      {
        var diffVariableObject = {};
        diffVariableObject.baseTex = "\\left( \\frac{\\partial " + data.variables[baseVariable].tex + "}{\\partial " + data.variables[diffVariable].tex + "} \\right)";
        diffVariableObject.baseTex += "_{\\mathrm{X}}";
        diffVariableObject.baseTex = "$$" + diffVariableObject.baseTex + "$$";
        diffVariableObject.combinationArray = [];

        deriveByVariableCombinations.forEach(function(combination) 
        {
          var combinationObject = {};
          combinationObject.combination = combination;
          combinationObject.tex = "";
          if (combination.indexOf(diffVariable) > -1) {
            combinationObject.isInvalidCombination = true;
          }
          var matchedDifferential = data.variables[baseVariable].differentials.find(function(differential) 
          {
            return  differential.by == diffVariable && 
                    (differential.const && differential.const.indexOf(combination[0]) > -1 && differential.const.indexOf(combination[1]) > -1) 
                    //  || differential.multipleConst TODO add multiple const support
          });
          combinationObject.tex = matchedDifferential ? matchedDifferential.isTex : "";
          diffVariableObject.combinationArray.push(combinationObject);
        });
        baseObject[diffVariable] = diffVariableObject;
      });
      tableData[baseVariable] = baseObject;
    });

    console.log(tableData);

    return tableData;
  }

  function createDifferentialTex(variableTex, differenitateByTex, constantsTex)
  {

  }
}
})();

